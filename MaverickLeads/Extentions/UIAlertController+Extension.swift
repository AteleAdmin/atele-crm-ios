//
//  UIAlertController+Extension.swift
//  MaverickLeads
//
//  Created by Developer  on 23/04/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

extension UIAlertController {
    /// SwifterSwift: Add a text field to Alert
    ///
    /// - Parameters:
    ///   - text: text field text (default is nil)
    ///   - placeholder: text field placeholder text (default is nil)
    ///   - textColor: text field color default is black
    ///   - font: custom font: default is system 17
    ///   - editingChangedTarget: an optional target for text field's editingChanged
    ///   - editingChangedSelector: an optional selector for text field's editingChanged
    public func addTextField(text: String? = nil, placeholder: String? = nil, textColor: UIColor = .black, font: UIFont = UIFont.systemFont(ofSize: 17), maxLength:Int = Int.max, editingChangedTarget: Any?, editingChangedSelector: Selector?, keyboardType: UIKeyboardType = .default, isSecureTextEntry: Bool = false) {
        addTextField { textField in
            textField.text = text
            textField.placeholder = placeholder
            textField.maxLength = maxLength
            if isSecureTextEntry {
                textField.isSecureTextEntry = true
            }
            textField.keyboardType = keyboardType
            if let target = editingChangedTarget, let selector = editingChangedSelector {
                textField.addTarget(target, action: selector, for: .editingChanged)
            }
        }
    }
}
