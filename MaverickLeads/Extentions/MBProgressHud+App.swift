//
//  MBProgressHud+App.swift
//  MaverickLeads
//
//  Created by Developer on 28/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import MBProgressHUD
import SnapKit

extension MBProgressHUD {
    
    // MARK: - Private Methods
    
    private static var animatedImageView: UIImageView? = nil
    
    
    // MARK: - Public Methods
    
    @discardableResult
    static func showLoadingHudAddedTo(view: UIView, title: String? = nil, animated: Bool = true) -> MBProgressHUD? {
        
        if let _ = view.viewWithTag(77676) {
            return nil
        }
        
        // Setup Progress HUD
        let progressHud = MBProgressHUD(view: view)
        view.addSubview(progressHud)
        progressHud.removeFromSuperViewOnHide = true
        progressHud.tag = 77676

        progressHud.show(animated: animated)
        
        return progressHud
    }
    
}
