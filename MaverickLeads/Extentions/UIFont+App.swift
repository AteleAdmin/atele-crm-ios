//
//  UIFont+extension.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

enum FontTypes {
    case text, regular, medium, semiBold, bold, light, thin, title
    func getFontName() -> String {
        switch self {
        case .text:
            return "IBMPlexSans-Text"
        case .regular:
            return "IBMPlexSans"
        case .medium:
            return "IBMPlexSans-Medium"
        case .semiBold:
            return "IBMPlexSans-Semibold"
        case .bold:
            return "IBMPlexSans-Bold"
        case .light:
            return "IBMPlexSans-Light"
        case .thin:
            return "IBMPlexSans-Thin"
        case .title:
            return "Edmund-Free"
        }
    }
}

protocol A {
    func get(of size: CGFloat) -> UIFont
    func name() -> String
}

extension A {
    func get(of size: CGFloat) -> UIFont {
        if let font = UIFont(name: name(), size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
}

class FontType {
    
    
    enum IBM: String, A {
        case text = "IBMPlexSans-Text",
        regular = "IBMPlexSans-Regular"

        func name() -> String {
            return self.rawValue
        }
    }
    
    enum Roboto: String, A {
        case text = ""
        func name() -> String {
            return self.rawValue
        }
    }
}

extension UIFont {
    
    class func appFront(ofSize size: CGFloat, type: FontTypes = .regular) -> UIFont {
        if let font = UIFont(name: type.getFontName(), size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
}
