//
//  UtilityExtensions.swift
//  MaverickLeads
//
//  Created by Developer  on 10/05/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import UIKit

extension DispatchTime {
    
    static func executeAfter(seconds: TimeInterval, completionHandler: @escaping () -> ()) {
        let mainQueue = DispatchQueue.main
        let deadline = DispatchTime.now() + seconds
        
        mainQueue.asyncAfter(deadline: deadline) {
            completionHandler()
        }
    }
    
    static func executeAfter(milliSeconds: TimeInterval, completionHandler: @escaping () -> ()) {
        let mainQueue = DispatchQueue.main
        let deadline = DispatchTime.now() + .milliseconds(Int(milliSeconds))
        
        mainQueue.asyncAfter(deadline: deadline) {
            completionHandler()
        }
    }
}

extension Collection {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension UIApplication {
    class var notificationBadgeCount: Int {
        get {
            return UIApplication.shared.applicationIconBadgeNumber
        }
        
        set {
            UIApplication.shared.applicationIconBadgeNumber = newValue
        }
    }
    
    class var hasNotifications: Bool {
        return notificationBadgeCount > 0
    }
}

extension Float {
    var clean: String {
        return String(format: "%.2f", self)
    }
}

extension Double {
    var clean: String {
        return isInteger ? String(format: "%.0f", self) : String(format: "%.2f", self)
    }
    
    var isInteger: Bool {
        return floor(self) == self
    }
    
    var to2Decimal: String {
        return String(format: "%.2f", self)
    }
}

extension Int {
    var clean: String {
        return String(self)
    }
}

extension Array {
    
    func shift(withDistance distance: Int = 1) -> Array<Element> {
        let offsetIndex = distance >= 0 ?
            self.index(startIndex, offsetBy: distance, limitedBy: endIndex) :
            self.index(endIndex, offsetBy: distance, limitedBy: startIndex)
        
        guard let index = offsetIndex else { return self }
        return Array(self[index ..< endIndex] + self[startIndex ..< index])
    }
    
    mutating func shiftInPlace(withDistance distance: Int = 1) {
        self = shift(withDistance: distance)
    }
    
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}

class LinkedLabel: UILabel {

fileprivate let layoutManager = NSLayoutManager()
fileprivate let textContainer = NSTextContainer(size: CGSize.zero)
fileprivate var textStorage: NSTextStorage?


override init(frame aRect:CGRect){
    super.init(frame: aRect)
    self.initialize()
}

required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.initialize()
}

func initialize(){

    let tap = UITapGestureRecognizer(target: self, action: #selector(LinkedLabel.handleTapOnLabel))
    self.isUserInteractionEnabled = true
    self.addGestureRecognizer(tap)
}

override var attributedText: NSAttributedString?{
    didSet{
        if let _attributedText = attributedText{
            self.textStorage = NSTextStorage(attributedString: _attributedText)

            self.layoutManager.addTextContainer(self.textContainer)
            self.textStorage?.addLayoutManager(self.layoutManager)

            self.textContainer.lineFragmentPadding = 0.0;
            self.textContainer.lineBreakMode = self.lineBreakMode;
            self.textContainer.maximumNumberOfLines = self.numberOfLines;
        }

    }
}

    @objc func handleTapOnLabel(tapGesture:UITapGestureRecognizer){

    let locationOfTouchInLabel = tapGesture.location(in: tapGesture.view)
    let labelSize = tapGesture.view?.bounds.size
    let textBoundingBox = self.layoutManager.usedRect(for: self.textContainer)
    let textContainerOffset = CGPoint(x: ((labelSize?.width)! - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: ((labelSize?.height)! - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

    let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
    let indexOfCharacter = self.layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: self.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)


        self.attributedText?.enumerateAttribute(NSAttributedString.Key.link, in: NSMakeRange(0, (self.attributedText?.length)!), options: NSAttributedString.EnumerationOptions(rawValue: UInt(0)), using:{
        (attrs: Any?, range: NSRange, stop: UnsafeMutablePointer<ObjCBool>) in

        if NSLocationInRange(indexOfCharacter, range){
            if let _attrs = attrs{

                UIApplication.shared.open(URL(string: _attrs as! String)!, options: [:], completionHandler: nil)
            }
        }
    })

}}
