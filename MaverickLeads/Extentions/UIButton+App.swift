//
//  UIButton+App.swift
//  MaverickLeads
//
//  Created by Developer on 06/05/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

extension UIButton {
    
    func addTextSpacing(_ letterSpacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: letterSpacing, range: NSRange(location: 0, length: (self.titleLabel?.text!.count)!))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    func set(title: String,
             textColor: UIColor = .white,
             backgroundColor: UIColor = .clear,
             font: UIFont = UIFont.appFront(ofSize: 12, type: .bold),
             letterSpacing: CGFloat = 1.0,
             for state: UIControl.State = .normal) {
        
        let attributes: [NSAttributedString.Key : Any] = [.font : font, .foregroundColor: textColor, .kern : letterSpacing]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        setAttributedTitle(attributedTitle, for: state)
        self.backgroundColor = backgroundColor
    }
    
}
