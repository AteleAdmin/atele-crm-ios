//
//  Encodable+App.swift
//  MaverickLeads
//
//  Created by Developer on 22/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

extension Encodable {
    
    func toDictionary(keysToChange: [String: String] = [:]) -> [String : Any]? {
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
            if var dict = json as? [String : Any] {
                keysToChange.forEach {
                    let value = dict[$0.key]
                    dict.removeValue(forKey: $0.key)
                    dict[$0.value] = value
                }
                return dict
            }
        } catch (let error) {
            print("To dictionary error: \(error)")
        }
        return nil
    }
}
