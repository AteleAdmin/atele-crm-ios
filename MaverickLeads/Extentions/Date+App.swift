//
//  Date+App.swift
//  MaverickLeads
//
//  Created by Developer on 20/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

extension Date {
    
    func getString(of type: DateFormatType, addTimeZone: Bool = false) -> String {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        
        if addTimeZone {
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
        }
        
        formatter.dateFormat = type.getFormat()
        return formatter.string(from: self)
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func offsetFrom(date : Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second, .month, .year]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m"
        let hours = "\(difference.hour ?? 0)h"
        let days = "\(difference.day ?? 0)d"
        let years = "\(difference.year ?? 0)y"
        let timeline = "\(difference.day ?? 0)/\(difference.month ?? 0)"
        
        if let year = difference.year, year       > 0 { return years }
        if let month = difference.month, month    > 0 { return timeline }
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    func fullOffsetFrom(date : Date) -> String {
        let timeInterval = timeIntervalSince1970
        let dateInterval = date.timeIntervalSince1970
        let difference = timeInterval - dateInterval
        return stringFromTimeInterval(interval: difference)
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        
        let interval = Int(interval)
        let days = (interval % 31536000) / 86400
        let hours = (interval % 86400) / 3600
        let minutes = (interval % 3600) / 60
        
        var string = ""
        
        if days > 0 {
            string += "\(days) "
            string += days > 1 ? "days " : "day "
        } else {
            if hours > 0 {
                string += "\(hours) "
                string += hours > 1 ? "hours " : "hour "
            }
            
            if minutes > 0 {
                string = hours > 0 ? "\(string)& " : string
                string += "\(minutes) "
                string += minutes > 1 ? "minutes" : "minute"
            }
        }
        
        return string.clean
    }
    
}

enum DateFormatType {
    case dayName, dayOfMonth, month, monthShort, year, timezone, forBackend, formatted, monthYear, dateTimeShort, time, dayMonthYear, navigationTitle, monthDate, dayOfWeek, shortDayOfWeek, hour, longDateTime,  shortMonthDateYear
    
    func getFormat() -> String {
        switch self {
        case .dayName:
            return "EEEE"
        case .dayOfMonth:
            return "dd"
        case .month:
            return "MM"
        case .monthShort:
            return "MMM"
        case .year:
            return "yyyy"
        case .forBackend:
            return "yyyy-MM-dd'T'HH:mm:ss.000Z"
        case .timezone:
            return "yyyy-MM-dd'T'HH:mm:sssZ"
        case .formatted:
            return "EEE, MMM d, yyyy"
        case .navigationTitle:
            return "EEE, MMM d"
        case .monthDate:
            //return "EEE, MMMM d"
            return "MMMM d"
        case .monthYear:
            return "MM/yyyy"
        case .dateTimeShort:
            return "MMMM d, hh:mm a"
        case .time:
            return "hh:mm a"
        case .dayMonthYear:
            return "yyyy-MM-dd"
        case .dayOfWeek:
            return "EEEE, MMMM d"
        case .shortDayOfWeek:
            return "EEE, MMMM d"
        case .hour:
            return "HH"
        case .longDateTime:
            return "yyyy-MM-dd HH:mm:ss"
        case .shortMonthDateYear:
            return "MM / dd / yy"
        }
    }
    
    func getFormatter(ignoreTimeZone: Bool = false) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        if ignoreTimeZone == false  {
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
        }
        formatter.dateFormat = getFormat()
        return formatter
    }

}

extension String {
    func toDate(format: DateFormatType, ignoreTimeZone: Bool = false) -> Date? {
        return format.getFormatter(ignoreTimeZone: ignoreTimeZone).date(from: self)
    }
}

