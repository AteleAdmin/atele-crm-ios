//
//  UICollectionView+App.swift
//  MaverickLeads
//
//  Created by Developer on 09/04/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func setEmptyBackgroundView(text: String) {
        let emptyLabel = UILabel(frame: frame)
        emptyLabel.text = text
        emptyLabel.textAlignment = .center
        emptyLabel.font = UIFont.appFront(ofSize: 12)
        emptyLabel.textColor = .darkGray
        emptyLabel.numberOfLines = 0
        emptyLabel.sizeToFit()
        
        backgroundView = emptyLabel
    }
    
    func resetBackgroundView() {
        backgroundView = UIView(frame: .zero)
    }
    
    func getCell<T: Registerable>(type: T.Type, for indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: type.identifier, for: indexPath) as? T
    }
    
    func register(types: Registerable.Type ...) {
        for type in types {
            print("type: \(type), identifier: \(type.identifier)")
            register(type.getNIB(), forCellWithReuseIdentifier: type.identifier)
        }
    }
}
