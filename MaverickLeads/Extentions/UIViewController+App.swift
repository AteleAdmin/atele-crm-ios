//
//  UIViewController+extension.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func instanceFromStoryboard<T: UIViewController>(storyboard: UIStoryboard) -> T {
        let viewController = storyboard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
        
        return viewController
    }
    
    func push<T: UIViewController>(viewController: T.Type, storyboard: UIStoryboard, configurator: ((T) -> Void)? = nil) {
        let viewController:T = instanceFromStoryboard(storyboard: storyboard)
        configurator?(viewController)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setUpperCasedTitle(_ titleString: String) {
        title = titleString.uppercased()
    }
    
    static var topViewController: UIViewController {
        get {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while ((topController.presentedViewController) != nil) {
                    topController = topController.presentedViewController!
                }
                return topController
            }
            return UIViewController()
        }
    }
    
    func visibleViewController() -> UIViewController? {
        if let navigationController = navigationController {
            if navigationController.topViewController is UINavigationController {
                return navigationController.topViewController?.visibleViewController()
            } else {
                return navigationController.topViewController
            }
        } else {
            return self
        }
    }
    
    func getButtons(with properties: [BarButtonsProperties]) -> [UIBarButtonItem] {
        var barbuttons: [UIBarButtonItem] = []
        
        for property in properties {
            var barButton: UIBarButtonItem!
            
            if let text = property.text, !text.isEmpty {
                barButton = UIBarButtonItem(title: property.text, style: .plain, target: self, action: property.action)
                barButton.image = property.image
            } else {
                let button = UIButton(type: .system)
                button.setImage(property.image, for: .normal)
                button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
                if let action = property.action {
                    button.addTarget(self, action: action, for: .touchUpInside)
                }
                barButton = UIBarButtonItem(customView: button)
            }
            barButton.tag = property.tag
            
            barbuttons.append(barButton)
        }
        return barbuttons
    }
    
    @discardableResult
    func addRightBarButtons(properties: BarButtonsProperties ...) -> [UIBarButtonItem] {
        var newButtons = getButtons(with: properties)
        if let previousButtons = self.navigationItem.rightBarButtonItems {
            let alreadyAddedButtons = previousButtons.filter { item in
                newButtons.first { $0.tag == item.tag } != nil
            }
            alreadyAddedButtons.forEach { item in
                newButtons.removeAll {
                    $0.tag == item.tag
                }
            }
            newButtons.append(contentsOf: previousButtons)
        }
        self.navigationItem.rightBarButtonItems = newButtons
        
        return newButtons
    }
    
    func removeBarButton(with tags: Int...) {
        var leftItems = navigationItem.leftBarButtonItems ?? []
        var rightItems = navigationItem.rightBarButtonItems ?? []
        tags.forEach { tag in
            leftItems.removeAll { $0.tag == tag }
            rightItems.removeAll { $0.tag == tag }
        }
        navigationItem.leftBarButtonItems = leftItems
        navigationItem.rightBarButtonItems = rightItems
    }
    
    func appendLeftBarButtons(properties: BarButtonsProperties ...) {
        var newButtons: [UIBarButtonItem] = []
        if let previousButtons = self.navigationItem.leftBarButtonItems {
            newButtons = previousButtons
        }
        var buttons = getButtons(with: properties)
        let backButton = buttons.first { $0.tag == 1 }
        if let backButton = backButton {
            buttons.removeAll { $0 == backButton }
            newButtons.insert(backButton, at: 0)
        }
        newButtons.append(contentsOf: buttons)
        self.navigationItem.leftBarButtonItems = newButtons
    }
    
    @discardableResult
    func addLeftBarButtons(properties: BarButtonsProperties ...) -> [UIBarButtonItem] {
        let buttons = getButtons(with: properties)
        self.navigationItem.leftBarButtonItems = buttons
        return buttons
    }
    
    func addTitleLeftAligned(titleString: String) {
        // Main title should be cleared out
        title = ""
        
        // Apply custom font settings
        let titleLable = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        titleLable.font = UIFont.appFront(ofSize: 18, type: .title)
        titleLable.textColor = .white
        titleLable.text = titleString.uppercased()
        
        var newBarButtons: [UIBarButtonItem] = []
        if let previousButtons = self.navigationItem.leftBarButtonItems {
            newBarButtons = previousButtons
        }
        
        let customBarButton = UIBarButtonItem(customView: titleLable)
        newBarButtons.append(customBarButton)
        self.navigationItem.leftBarButtonItems = newBarButtons
    }
    
    class BarButtonsProperties {
        var image: UIImage?
        var text: String?
        var action: Selector?
        var tintColor: UIColor = .white
        var tag: Int = 90009
        init(image img: UIImage? = nil, text txt: String? = nil, action sel: Selector? = nil, tintColor color: UIColor = UIColor.white, tag buttonTag: Int = 90009) {
            image = img
            text = txt
            action = sel
            tintColor = color
            tag = buttonTag
        }
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func animate(from fromViewController: UIViewController?,
                 to toViewController: UIViewController,
                 in containerView: UIView,
                 completion: (() -> Void)? = nil) {
        let finalRect = containerView.bounds
        
        if fromViewController == nil {
            addChild(toViewController)
            toViewController.view.frame = finalRect
            
            containerView.addSubview(toViewController.view)
            
            toViewController.didMove(toParent: self)
            
            if completion != nil {
                completion!()
            }
            
        } else if (fromViewController != toViewController) {
            fromViewController?.willMove(toParent: self)
            addChild(toViewController)
            toViewController.view.frame = finalRect
            toViewController.view.alpha = 0.0
            
            containerView.addSubview(toViewController.view)
            
            transition(from: fromViewController!,
                       to: toViewController,
                       duration: 0.2,
                       options: UIView.AnimationOptions.curveLinear,
                       animations: {
                        toViewController.view.alpha = 1.0
                        
            }, completion: { (_ finished) in
                fromViewController?.view.removeFromSuperview()
                fromViewController?.removeFromParent()
                
                toViewController.didMove(toParent: self)
                
                if completion != nil {
                    completion!()
                }
            })
        }
    }
    
    private static let availableViewTag = 1218
    
    func showCustomViewBeforDrafting(with text: String) {
        hideCustomViewBeforDrafting()
        let availableView = UIView(frame: view.bounds)
        availableView.backgroundColor = .white
        availableView.tag = UIViewController.availableViewTag
        view.addSubview(availableView)
        view.bringSubviewToFront(availableView)
        availableView.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.top.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        let descriptionLabel = UILabel(frame: .zero)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = text
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont.appFront(ofSize: 14, type: .text)
        availableView.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    func hideCustomViewBeforDrafting() {
        if let availableView = view.viewWithTag(UIViewController.availableViewTag) {
            availableView.removeFromSuperview()
        }
    }
    
    class func swapRootViewController(_ viewController: UIViewController, animationType: SwapRootVCAnimationType, completion: (() -> ())? = nil) {
        UIApplication.shared.keyWindow?.swapRootViewControllerWithAnimation(newViewController: viewController, animationType: animationType, completion: completion)
    }
    
    func performAnimation(animation: @escaping () -> Void) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let self = self else { return }
            animation()
            self.view.layoutIfNeeded()
        }
    }
    
    func performAnimation(duration: Double = 0.3, options: UIView.AnimationOptions, animation: @escaping () -> Void, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, delay: 0.0, options: options, animations: animation, completion: completion)
    }
    
    private static let insetBackgroundViewTag = 98721 //Cool number
    
    func paintSafeAreaBottomInset(withColor color: UIColor) {
        guard #available(iOS 11.0, *) else {
            return
        }
        if let insetView = view.viewWithTag(UIViewController.insetBackgroundViewTag) {
            insetView.backgroundColor = color
            if color == UIColor.clear {
                view.sendSubviewToBack(insetView)
            } else {
                view.bringSubviewToFront(insetView)
            }
            return
        }
        
        let insetView = UIView(frame: .zero)
        insetView.tag = UIViewController.insetBackgroundViewTag
        insetView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(insetView)
        //view.sendSubview(toBack: insetView)
        
        if color == UIColor.clear {
            view.sendSubviewToBack(insetView)
        } else {
            view.bringSubviewToFront(insetView)
        }
        insetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        insetView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        insetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        let topConstraint = insetView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        topConstraint.isActive = true
        insetView.backgroundColor = color
    }
    
    func addBlurBackground() {
        view.isOpaque = false
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.7
        view.addSubview(blurEffectView)
        view.sendSubviewToBack(blurEffectView)
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }
}

extension UIBarButtonItem {
    
    func fontsSettings(color: UIColor = .white, disabledColor: UIColor = .lightGray) {
        tintColor = color
        let arr = [NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                   NSAttributedString.Key.font: UIFont.appFront(ofSize: 14, type: .bold)]
        setTitleTextAttributes(arr, for: .highlighted)
        setTitleTextAttributes(arr, for: .disabled)
        setTitleTextAttributes(arr, for: .selected)
        setTitleTextAttributes(arr, for: .focused)
        setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color,
                                NSAttributedString.Key.font: UIFont.appFront(ofSize: 14, type: .bold)], for: .normal)
    }
}

