//
//  UITableView+App.swift
//  MaverickLeads
//
//  Created by Developer on 09/04/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

extension UITableView {
    
    func setEmptyBackgroundView(text: String) {
        let emptyLabel = UILabel(frame: frame)
        emptyLabel.text = text
        emptyLabel.textAlignment = .center
        emptyLabel.font = UIFont.appFront(ofSize: 12, type: .regular)
        emptyLabel.textColor = .darkGray
        emptyLabel.numberOfLines = 0
        emptyLabel.sizeToFit()
        emptyLabel.isEnabled = false
        
        backgroundView = emptyLabel
    }
    
    func resetBackgroundView() {
        backgroundView = UIView(frame: .zero)
    }
    
    func register(types: Registerable.Type ...) {
        for type in types {
            print("type: \(type), identifier: \(type.identifier)")
            register(type.getNIB(), forCellReuseIdentifier: type.identifier)
        }
    }
    
    func getCell<T: Registerable>(type: T.Type) -> T? {
        return dequeueReusableCell(withIdentifier: type.identifier) as? T
    }
    
}
