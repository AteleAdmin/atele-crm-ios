//
//  UIImageView+App.swift
//  MaverickLeads
//
//  Created by Developer on 21/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import AlamofireImage

extension UIImageView {
    
    private func image(urlString: String, phImage: UIImage? = nil, showIndicator: Bool = true, backgroundColor bgColor: UIColor = .clear, greyScaled: Bool = false) {
        guard let url = URL(string: urlString) else { return }
        
        var activityIndicator:UIActivityIndicatorView?
        
        if showIndicator {
            activityIndicator = UIActivityIndicatorView(style: .white)
            activityIndicator!.frame = bounds
            activityIndicator!.color = .white
            activityIndicator!.hidesWhenStopped = true
            addSubview(activityIndicator!)
            bringSubviewToFront(activityIndicator!)
            activityIndicator!.startAnimating()
        }
        backgroundColor = bgColor
        af_setImage(withURL: url, placeholderImage: phImage) { [weak self] response in
            guard let self = self else { return }
            activityIndicator?.stopAnimating()
            activityIndicator?.removeFromSuperview()
            if greyScaled {
                self.greyScale()
            }
        }
    }
    
    func greyScale() {
        guard let image = self.image,
            let cgImage = image.cgImage,
            let filter = CIFilter(name: "CIPhotoEffectMono") else { return }
        let ciImage = CIImage(cgImage: cgImage)
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        guard let outputImage = filter.outputImage else { return }
        let context = CIContext()
        if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
            let processedImage = UIImage(cgImage: cgimg)
            self.image = processedImage
        }
    }
    
}

enum NFLImageSizes {
    case small, medium, original
}
