//
//  AppDelegate.swift
//  MaverickLeads
//
//  Created by Developer on 13/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit
import Instabug
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        CBNotificationManager.shared.configure()
        setupCustomAppearance()
        thirdPartySDKConfigurations(withLaunchOptions: launchOptions)
        setupRootViewController()
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           CBNotificationManager.shared.notificationManager(didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
      }
      
      func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
          CBNotificationManager.shared.notificationManager(didFailToRegisterForRemoteNotificationsWithError: error)
      }
      
      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
          // Print full message.
          print(userInfo)
      }
      
      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
          
          CBNotificationManager.shared.didReceiveRemoteNotification(userInfo, fetchCompletionHandler: completionHandler)
      }
      
}


// MARK: - Private Methods

extension AppDelegate {
    
    private func setupCustomAppearance() {
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.window?.backgroundColor = .white
        
        // Sets background to a blank/empty image
//           UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
           // Sets shadow (line below the bar) to a blank image
           UINavigationBar.appearance().shadowImage = UIImage()
           // Sets the translucent background color
        UINavigationBar.appearance().barTintColor = UIColor.appMain
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
           // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = false
        
        UINavigationBar.appearance().tintColor = .white
    }
    
    private func thirdPartySDKConfigurations(withLaunchOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        IQKeyboardManager.shared.enable = true
    }
    
    private func setupRootViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if let user = AppUserDefaults.user.get() {
            let loginNavVC = UINavigationController()
            let loginVC: LoginViewController = loginNavVC.instanceFromStoryboard(storyboard: AppConstants.Storyboards.login)
            let homeVC: HomeViewController = loginNavVC.instanceFromStoryboard(storyboard: AppConstants.Storyboards.leads)
            homeVC.user = user
            loginNavVC.setViewControllers([loginVC, homeVC], animated: false)
            window?.rootViewController = loginNavVC
        } else {
            window?.rootViewController = AppConstants.Storyboards.login.instantiateInitialViewController()
        }
        
        window?.makeKeyAndVisible()
    }
    
}
