//
//  MLCountResponse.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation


class MLCountResponse: Codable {
    
    let name: String?
    let total: Int
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case total = "Total"
    }
    
    init(name: String, total: Int) {
        self.name = name
        self.total = total
    }
}
