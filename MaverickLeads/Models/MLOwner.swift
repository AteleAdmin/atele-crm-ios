//
//  MLOwner.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

class MLOwner: Codable {
    let id: Int
    let name, username, email, make: String
    let year, model, gender, hOwnerShip: String
    let address, agent, disposition, agency: String
    let transferTo, city, state, zip: String
    let phoneNo, company, dob: String
    let isActive: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case username = "Username"
        case email = "Email"
        case make = "Make"
        case year = "Year"
        case model = "Model"
        case gender = "Gender"
        case hOwnerShip = "HOwnerShip"
        case address = "Address"
        case agent = "Agent"
        case disposition = "Disposition"
        case agency = "Agency"
        case transferTo = "TransferTo"
        case city = "City"
        case state = "State"
        case zip = "Zip"
        case phoneNo = "PhoneNo"
        case company = "Company"
        case dob = "DOB"
        case isActive = "IsActive"
    }
    
    init(id: Int, name: String, username: String, email: String, make: String, year: String, model: String, gender: String, hOwnerShip: String, address: String, agent: String, disposition: String, agency: String, transferTo: String, city: String, state: String, zip: String, phoneNo: String, company: String, dob: String, isActive: Bool) {
        self.id = id
        self.name = name
        self.username = username
        self.email = email
        self.make = make
        self.year = year
        self.model = model
        self.gender = gender
        self.hOwnerShip = hOwnerShip
        self.address = address
        self.agent = agent
        self.disposition = disposition
        self.agency = agency
        self.transferTo = transferTo
        self.city = city
        self.state = state
        self.zip = zip
        self.phoneNo = phoneNo
        self.company = company
        self.dob = dob
        self.isActive = isActive
    }
}
