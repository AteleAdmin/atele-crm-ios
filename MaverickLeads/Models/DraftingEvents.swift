//
//  DraftingEvents.swift
//  MaverickLeads
//
//  Created by Khaliq ur Rehman on 15/04/2019.
//  Copyright © 2019 Abdul Baseer Khan. All rights reserved.
//

import Foundation

struct DraftEvent {
    //To server events
    var name: String
    init(_ name: String) {
        self.name = name
    }
    
    static let ping = DraftEvent("draft:ping")
    static let join = DraftEvent("user:join")
    static let leave = DraftEvent("user:leave")
    static let autoDraft = DraftEvent("draft:autoDraft")//, ["teamId", "autoDraft"]
    static let getConfigs = DraftEvent("draft:getConfigs")
    static let getAllPlayers = DraftEvent("draft:getAllPlayers")
    static let getStatus = DraftEvent("draft:getStatus")
    static let getAllSelectedPlayers = DraftEvent("draft:getAllSelectedPlayers")
    static let getDraftedPlayers = DraftEvent("draft:getDraftedPlayers")
    static let getQueue = DraftEvent("draft:getQueue")
    static let playerPicked = DraftEvent("draft:playerPicked")
    static let addInQueue = DraftEvent("draft:addInQueue")
    static let removeFromQueue = DraftEvent("draft:removeFromQueue")
    static let swapInQueue = DraftEvent("draft:swapInQueue")
    
    // From server events
    static let pong = DraftEvent("draft:pong")
    static let teamJoined = DraftEvent("draft:teamJoined")
    static let teamLeft = DraftEvent("draft:teamLeft")
    static let autoDraftSelected = DraftEvent("draft:autoDraftSelected")//, ["teamId"]
    static let configs = DraftEvent("draft:configs")
    static let loadingCompleted = DraftEvent("draft:init")
    static let allPlayers = DraftEvent("draft:allPlayers")
    static let status = DraftEvent("draft:status")
    static let allSelectedPlayers = DraftEvent("draft:allSelectedPlayers")
    static let queue = DraftEvent("draft:queue")
    static let draftedPlayers = DraftEvent("draft:draftedPlayers")
    static let started = DraftEvent("draft:started")
    static let onPlayerPicked = DraftEvent("draft:onPlayerPicked")
    static let turnChanged = DraftEvent("draft:turnChanged")
    static let disconnect = DraftEvent("draft:disconnect")
    static let completed = DraftEvent("draft:completed")
    static var a = ABK<(leagueId: Int, message: String)>(name: "asd")
    static func tester() {
        
    }
}
class ABK<Value> {
    
    var name: String
    var keys: Value?
    
    init(name n: String) {
        name = n
    }
    
    
}
