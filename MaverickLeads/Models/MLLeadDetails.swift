//
//  MLLeadInfo.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation


class MLLeadDetails: Codable {
    // Response of Lead info
    
    let id: Int?
    let firstName, lastName, address, city: String?
    let state, zip, phoneNo, company: String?
    let make, make2, year, year2: String?
    let model, model2, dob, email: String?
    let continousCoverage, gender, disposition, hOwnerShip: String?
    let transferTo: String?
    let isActive: Bool
    let maritalStatus, agencyOwner, agencyProducer: String?
    let viciLeadID: Int?
    var uId: Int = 0
    var dispositionId: Int = 0
    var lastModifiedBy: Int = 0
    var followUpXDate: String = ""
    var comments: String = ""
    var agencyProducerId: Int?
    
    var hOwnerBoolString: String {
        if hOwnerShip?.lowercased() == "yes" || hOwnerShip?.lowercased() == "true" {
            return "True"
        } else {
            return "False"
        }
    }
    
    var maritalStatusBoolString: String {
        if maritalStatus?.lowercased() == "yes" || maritalStatus?.lowercased() == "true" {
            return "True"
        } else {
            return "False"
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case firstName = "FirstName"
        case lastName = "LastName"
        case address = "Address"
        case city = "City"
        case state = "State"
        case zip = "Zip"
        case phoneNo = "PhoneNo"
        case company = "Company"
        case make = "Make"
        case make2 = "Make2"
        case year = "Year"
        case year2 = "Year2"
        case model = "Model"
        case model2 = "Model2"
        case dob = "DOB"
        case email = "Email"
        case continousCoverage = "ContinousCoverage"
        case gender = "Gender"
        case disposition = "Disposition"
        case hOwnerShip = "HOwnerShip"
        case transferTo = "TransferTo"
        case isActive = "IsActive"
        case maritalStatus = "MaritalStatus"
        case agencyOwner = "AgencyOwner"
        case agencyProducer = "AgencyProducer"
        case agencyProducerId = "agencyProducerId"
        case viciLeadID = "ViciLeadId"
    }
    
    init(id: Int, firstName: String, lastName: String, address: String, city: String, state: String, zip: String, phoneNo: String, company: String, make: String, make2: String, year: String, year2: String, model: String, model2: String, dob: String, email: String, continousCoverage: String, gender: String, disposition: String, hOwnerShip: String, transferTo: String, isActive: Bool, maritalStatus: String, agencyOwner: String, agencyProducer: String, viciLeadID: Int) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.address = address
        self.city = city
        self.state = state
        self.zip = zip
        self.phoneNo = phoneNo
        self.company = company
        self.make = make
        self.make2 = make2
        self.year = year
        self.year2 = year2
        self.model = model
        self.model2 = model2
        self.dob = dob
        self.email = email
        self.continousCoverage = continousCoverage
        self.gender = gender
        self.disposition = disposition
        self.hOwnerShip = hOwnerShip
        self.transferTo = transferTo
        self.isActive = isActive
        self.maritalStatus = maritalStatus
        self.agencyOwner = agencyOwner
        self.agencyProducer = agencyProducer
        self.viciLeadID = viciLeadID
    }
    
    func getAsParams() -> [String: Any] {
        
        return [
            "fName": firstName ?? "",
            "lName": lastName ?? "",
            "city": city ?? "",
            "state": state ?? "",
            "zip": zip ?? "",
            "phone": phoneNo ?? "",
            "company": company ?? "",
            "make": make ?? "",
            "year": year ?? "",
            "model": model ?? "",
            "make2": make2 ?? "",
            "year2": year2 ?? "",
            "model2": model2 ?? "",
            "dob": dob ?? "",
            "email": email ?? "",
            "address": address ?? "",
            "comments": comments,
            "homeOwner": hOwnerShip ?? false,
            "continousCoverage": continousCoverage ?? "",
            "maritalStatus": maritalStatus ?? "false",
            "followUpXDate": followUpXDate,
            "dispositionId": dispositionId,
            "lastModifiedBy": lastModifiedBy,
            "id": id  ?? 0,
            "uId": uId,
            "agencyProducerId": agencyProducerId ?? 0// agency producer ki id daalni ha
        ]
        
    }
}
