//
//  MLUser.swift
//  MaverickLeads
//
//  Created by Developer on 10/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

class MLUser: Codable {
    let uID: Int
    let name, username, email: String?
    let roleID: Int
    let isLogged: Bool
    
    var role: String?
    
    var uRole: UserRole {
        return UserRole.roleFromString(role: role ?? "")
    }
    
    var getSuperAccess: [UserSuperAccess] {
        switch uRole {
        case .superAdmin:
            return [.liveLeads, .livePnp, .pingAndPost, .outsourcePanel, .logout]
        case .teamLead:
            return [.leads, .pingAndPost]
        case .agencyOwner:
            return [.liveTransfers, .pingAndPost, .livePnp, .logout]
        case .agencyProducer:
            return [.leads, .pingAndPost]
        }
    }
    
    var getSubAccess: [UserSuperAccess: [UserSubAccess]] {
        var subAccesses = [UserSuperAccess: [UserSubAccess]]()
        switch uRole {
        case .superAdmin:
            subAccesses[.livePnp] = [.agentReport, .tlNewLeads , .agencyReport]
            subAccesses[.liveLeads] = [.liveTransfers, .agentReport, .tlNewLeads , .agencyReport]
            subAccesses[.pingAndPost] = [.pingAndPost, .agencyReport, .tlNewLeads , .agentReport]
        case .teamLead:
            subAccesses[.leads] = [.tlNewLeads]
            subAccesses[.pingAndPost] = [.tlNewLeads ,.pnpAgencyReport, .pnpAgentReport]
        case .agencyOwner:
            break
        case .agencyProducer:
            break
        }
        
        return subAccesses
    }
    
    enum CodingKeys: String, CodingKey {
        case uID = "UId"
        case name = "Name"
        case username = "Username"
        case email = "Email"
        case role = "Role"
        case roleID = "RoleId"
        case isLogged = "IsLogged"
    }
}
