//
//  AppTargetEnvironment.swift
//  MaverickLeads
//
//  Created by Developer  on 29/10/2018.
//  Copyright © 2018 Developer . All rights reserved.
//

import Foundation

// MARK:- App Target

enum AppTarget: String {
    case development = "Development"
    case qa = "QA"
    case staging = "Staging"
    case appStore = "AppStore"
    
    static var current: AppTarget {
        
        #if DEVELOPEMENT
        return AppTarget.development
        
        #elseif STAGING
        return AppTarget.staging
        
        #elseif QA
        return AppTarget.qa
        
        #else
        return AppTarget.appStore
        
        #endif
    }
    
}
