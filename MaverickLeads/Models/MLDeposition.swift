//
//  MLDeposition.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

class MLDisposition: Codable {
    let id: Int
    let name: String
    let isActive: Bool
    let scopeID, createdBy: Int
    let createdDate: String
    let lastModifiedBy: Int
    let lastModifiedDate: String
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case isActive = "IsActive"
        case scopeID = "ScopeId"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case lastModifiedBy = "LastModifiedBy"
        case lastModifiedDate = "LastModifiedDate"
    }

}
