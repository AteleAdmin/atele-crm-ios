//
//  MLLead.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

class MLLead: Codable {
    let id: Int
    let name, address, city, state: String?
    let zip, phoneNo, company, make: String?
    let year, model, dob, email: String?
    let gender, hOwnerShip, isDemandPnp:String?
    let isActive: Bool
    let agent, disposition, agency, transferTo: String?
    let timeStamp: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case address = "Address"
        case city = "City"
        case state = "State"
        case zip = "Zip"
        case phoneNo = "PhoneNo"
        case company = "Company"
        case make = "Make"
        case year = "Year"
        case model = "Model"
        case dob = "DOB"
        case email = "Email"
        case gender = "Gender"
        case hOwnerShip = "HOwnerShip"
        case isActive = "IsActive"
        case agent = "Agent"
        case disposition = "Disposition"
        case agency = "Agency"
        case transferTo = "TransferTo"
        case timeStamp = "TimeStamp"
        case isDemandPnp = "IsDemandPNP"
    }
    
    init(id: Int, name: String, address: String, city: String, state: String, zip: String, phoneNo: String, company: String, make: String, year: String, model: String, dob: String, email: String, gender: String, hOwnerShip: String, isActive: Bool, agent: String, disposition: String, agency: String, transferTo: String, timeStamp: String, isDemandPNP: String) {
        self.id = id
        self.name = name
        self.address = address
        self.city = city
        self.state = state
        self.zip = zip
        self.phoneNo = phoneNo
        self.company = company
        self.make = make
        self.year = year
        self.model = model
        self.dob = dob
        self.email = email
        self.gender = gender
        self.hOwnerShip = hOwnerShip
        self.isActive = isActive
        self.agent = agent
        self.disposition = disposition
        self.agency = agency
        self.transferTo = transferTo
        self.timeStamp = timeStamp
        self.isDemandPnp = isDemandPNP
    }
}
