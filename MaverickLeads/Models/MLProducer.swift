//
//  MLProducer.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

class MLProducer: Codable {
    let id: Int?
    let name, username, email, password: String?
    let gender, contactNo, address, mlProducerDescription: String?
    let doj: String?
    let roleID, agencyOwnerID: Int?
    let isActive, isAccept: Bool?
    let createdBy: Int?
    let createdDate: String?
    let lastModifiedBy: Int?
    let updatedDate: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case username = "Username"
        case email = "Email"
        case password = "Password"
        case gender = "Gender"
        case contactNo = "ContactNo"
        case address = "Address"
        case mlProducerDescription = "Description"
        case doj = "DOJ"
        case roleID = "RoleId"
        case agencyOwnerID = "AgencyOwnerId"
        case isActive = "IsActive"
        case isAccept = "IsAccept"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case lastModifiedBy = "LastModifiedBy"
        case updatedDate = "UpdatedDate"
    }
    
    init(id: Int, name: String, username: String, email: String, password: String, gender: String, contactNo: String, address: String, mlProducerDescription: String, doj: String, roleID: Int, agencyOwnerID: Int, isActive: Bool, isAccept: Bool, createdBy: Int, createdDate: String, lastModifiedBy: Int, updatedDate: String) {
        self.id = id
        self.name = name
        self.username = username
        self.email = email
        self.password = password
        self.gender = gender
        self.contactNo = contactNo
        self.address = address
        self.mlProducerDescription = mlProducerDescription
        self.doj = doj
        self.roleID = roleID
        self.agencyOwnerID = agencyOwnerID
        self.isActive = isActive
        self.isAccept = isAccept
        self.createdBy = createdBy
        self.createdDate = createdDate
        self.lastModifiedBy = lastModifiedBy
        self.updatedDate = updatedDate
    }
}
