// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let datum = try? newJSONDecoder().decode(Datum.self, from: jsonData)

import Foundation

// MARK: - Datum
class NFLSeasonSchedule: Codable {
    let title: String
    let games: [Game]
    
    var hasUnSimulatedGame: Bool {
        return games.filter({$0.status == .simulate}).count > 0
    }
}

class NFLPushSeasonSchedule: Codable {
    let title: String
    let games: [PushGame]
    
    var hasUnSimulatedGame: Bool {
        return games.filter({$0.matchStatus == .scheduled}).count > 0
    }
    
    init() {
        title = ""
        games = []
    }
}
