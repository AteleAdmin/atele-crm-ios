//
//  MLAgencyCount.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

class MLAgencyCount: Codable {
    let name, state: String
    let total, accepted, rejected: Int
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case state = "State"
        case total = "Total"
        case accepted = "Accepted"
        case rejected = "Rejected"
    }
    
    init(name: String, state: String, total: Int, accepted: Int, rejected: Int) {
        self.name = name
        self.state = state
        self.total = total
        self.accepted = accepted
        self.rejected = rejected
    }
}
