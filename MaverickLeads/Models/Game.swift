// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let game = try? newJSONDecoder().decode(Game.self, from: jsonData)

import Foundation

// MARK: - Game

class Game: Codable {
    let title, id: String
    private let simulated: Bool
    
    var status: SeasonScheduleAction {
        return .simulate // simulated ? .simulated : .simulate
    }
}


enum MatchStatus: String {
    case scheduled, closed, postponed, inProgress, none
    
    var titleString: String {
        switch self {
        case .scheduled: return "SIMULATE"
        case .closed: return "CLOSED"
        case .inProgress: return "SIMULATING…"
        case .postponed: return "POSTPONED"
        case .none: return "UNKNOWN"
        }
    }
}

class PushGame: Codable {
    let title, id: String
    private let status: String
    
    var matchStatus: MatchStatus {
        return MatchStatus(rawValue: status) ?? .none
    }
}
