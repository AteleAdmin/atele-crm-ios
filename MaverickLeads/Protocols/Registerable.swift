//
//  Registerable.swift
//  MaverickLeads
//
//  Created by Developer on 31/12/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

protocol Registerable {
    static var identifier: String { get }
    static func getNIB() -> UINib
}

extension Registerable {

    static var identifier: String {
        return String(describing: self)
    }
    
    static func getNIB() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
