//
//  StatsCountTableViewCell.swift
//  MaverickLeads
//
//  Created by Zaeem Zafar on 19/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit

class StatsCountTableViewCell: UITableViewCell {
    @IBOutlet weak var leftLabel: UILabel!
    
    @IBOutlet weak var rightLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
