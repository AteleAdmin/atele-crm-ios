//
//  ConfettiView.swift
//  MaverickLeads
//
//  Created by Developer on 23/12/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

class CRConfettiView: UIView {
    
    
    // MARK: - Class Properties
    
    private let dimension = 4
    private var imagesNames = ["icn_circle", "icn_star", "icn_triangle"]
    private var colors: [UIColor] = [UIColor.hexColor("f72567"), UIColor.hexColor("2e5be2"), UIColor.hexColor("36AE36")]
    private let emitterLayer = CAEmitterLayer()
    
    
    // MARK: - Initialization Methods
    
    init() {
        super.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    // MARK: - Public Methods
    
    public func startAnimation() {
        showCelebrateAnimation()
    }
    
    public func stopAnimation() {
        emitterLayer.removeFromSuperlayer()
    }
    
    
    // MARK: - Private Methods
    
    private func showCelebrateAnimation() {
        emitterLayer.emitterPosition = CGPoint(x: bounds.width / 2, y: -50)
        emitterLayer.emitterSize = CGSize(width: bounds.width, height: 0)
        emitterLayer.emitterShape = .line
        emitterLayer.beginTime = CACurrentMediaTime()
        emitterLayer.timeOffset = 10
        emitterLayer.emitterCells = generateConfettiEmitterCells()
        layer.addSublayer(emitterLayer)
    }
    
    private func generateConfettiEmitterCells() -> [CAEmitterCell] {
        var cells = [CAEmitterCell]()
        
        for index in 0..<10 {
            let cell = CAEmitterCell()
            cell.color = nextColor(i: index)
            cell.contents = nextImage(i: index)
            cell.scale = 0.08
            cell.scaleRange = 0.3
            cell.emissionRange = .pi
            cell.lifetime = 20.0
            cell.birthRate = 15
            cell.velocity = -50
            cell.yAcceleration = 30
            cell.xAcceleration = 5
            cell.spin = -0.5
            cell.spinRange = 1.0
            
            cells.append(cell)
        }
        
        return cells
    }
    
    private func nextColor(i: Int) -> CGColor {
        return (colors[safe: i % dimension] ?? UIColor.red).cgColor
    }
    
    private func nextImage(i: Int) -> CGImage? {
        return UIImage(named: imagesNames[safe: i % dimension] ?? "")?.cgImage
    }
    
    private func randomNumber() -> Int {
        return Int(arc4random_uniform(UInt32(dimension)))
    }
    
}
