//
//  ExportTableViewCell.swift
//  MaverickLeads
//
//  Created by Developer on 11/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit

enum CellActionMode {
    case phone, email
}

class ExportTableViewCell: UITableViewCell {

    @IBOutlet weak var serialLabel: UILabel!
    @IBOutlet weak var dispositionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var productLabel: UILabel!
    
    var callAction: ((_ action: CellActionMode) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func emailButtonTapped(_ sender: Any) {
        callAction?(.email)
    }
    
    @IBAction func phoneButtonTapped(_ sender: Any) {
        callAction?(.phone)
    }
    
    func configureCell(lead: MLLead, completion: ((CellActionMode) -> Void)? = nil ) {
        serialLabel.text = String(lead.id)
        dispositionLabel.text = lead.disposition
        nameLabel.text = lead.name
        stateLabel.text = lead.state
        cityLabel.text = lead.city ?? ""
        zipLabel.text = lead.zip
        phoneLabel.text = lead.phoneNo
        emailLabel.text = lead.email
        phoneLabel.text = lead.phoneNo
        productLabel.text = lead.isDemandPnp ?? "-"
        
        callAction = completion
    }
}
