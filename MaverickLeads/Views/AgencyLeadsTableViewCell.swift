//
//  AgencyLeadsTableViewCell.swift
//  MaverickLeads
//
//  Created by Developer on 14/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit

class AgencyLeadsTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var acceptedLabel: UILabel!
    @IBOutlet weak var rejectedTotal: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(forIndexPath indexPath: IndexPath, source: [MLLead]) {
        
        if indexPath.row == 0 {
            nameLabel.text = "Name"
            stateLabel.text = "State"
            acceptedLabel.text = "Accepted"
            rejectedTotal.text = "Rejected"
            totalLabel.text = "Total"
            
            nameLabel.font = .boldSystemFont(ofSize: 13)
            stateLabel.font = .boldSystemFont(ofSize: 13)
            acceptedLabel.font = .boldSystemFont(ofSize: 13)
            rejectedTotal.font = .boldSystemFont(ofSize: 13)
            totalLabel.font = .boldSystemFont(ofSize: 13)
        } else {
            nameLabel.textColor = .gray
            stateLabel.textColor = .gray
            acceptedLabel.textColor = .gray
            rejectedTotal.textColor = .gray
            totalLabel.textColor = .gray
            
            let accepted = source.filter({($0.disposition?.isRejected ?? false) == false}).count
            
            nameLabel.text = source.first?.agency ?? ""
            stateLabel.text = source.first?.state ?? ""
            acceptedLabel.text = "\(accepted)"
            rejectedTotal.text = "\(source.count - accepted)"
            totalLabel.text = "\(source.count)"
        }
        
    }
}
