//
//  AccessCollectionViewCell.swift
//  MaverickLeads
//
//  Created by Developer on 10/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit

class AccessCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var accessImage: UIImageView!
    @IBOutlet weak var accessLabel: UILabel!
}
