//
//  EditableFormViewController.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit
import DatePickerDialog

class EditableFormViewController: UITableViewController {

    @IBOutlet weak var producerEmailTextField: DropDown!
    @IBOutlet weak var dispositionTextField: DropDown!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var genderTextField: DropDown!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var followUpDateTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var homeOwnerTextField: DropDown!
    @IBOutlet weak var maritalStatusTextField: DropDown!
    
    @IBOutlet weak var v1YearTextField: UITextField!
    @IBOutlet weak var v1MakeTextField: UITextField!
    @IBOutlet weak var v1ModelTextField: UITextField!
    
    @IBOutlet weak var v2YearTextField: UITextField!
    @IBOutlet weak var v2MakeTextField: UITextField!
    @IBOutlet weak var v2ModelTextField: UITextField!
    @IBOutlet weak var currentIssueTextField: UITextField!
    
    @IBOutlet weak var lengthCoverageTextField: DropDown!
    @IBOutlet weak var noteTextField: UITextField!
    
    var leadDetail: MLLeadDetails!
    var apiCategory: APICategory!
    
    var dispositionList: [MLDisposition] {
        return AppUserDefaults.dispositionList.get() ?? [MLDisposition]()
    }
    
    var producerList: [MLProducer] {
        return AppUserDefaults.producerList.get() ?? [MLProducer]()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    func setupView() {
        producerEmailTextField.optionArray = producerList.compactMap({$0.name})
        dispositionTextField.optionArray = dispositionList.compactMap({$0.name})
        genderTextField.optionArray = [ "Male", "Female"]
        maritalStatusTextField.optionArray = ["True", "False"]
        lengthCoverageTextField.optionArray = ["Continous:", "Less than 1 year", "1-2 years", "3-5years", "5+ years"]
        homeOwnerTextField.optionArray = ["True", "False"]
        
        producerEmailTextField.inputView = UIView()
        dispositionTextField.inputView = UIView()
        genderTextField.inputView = UIView()
        maritalStatusTextField.inputView = UIView()
        lengthCoverageTextField.inputView = UIView()
        homeOwnerTextField.inputView = UIView()
        dobTextField.inputView = UIView()
        followUpDateTextField.inputView = UIView()
        
//        genderTextField.optionArray = ["Gender:", "Male", "Female"]
//        maritalStatusTextField.optionArray = ["Marital Status:", "true", "false"]
//        lengthCoverageTextField.optionArray = ["Continous:", "Less than 1 year", "1-2 years", "3-5years", "5+ years"]
//        homeOwnerTextField.optionArray = ["Home Owner:", "true", "false"]
        
        producerEmailTextField.text = leadDetail.agencyProducer
        dispositionTextField.text = leadDetail.disposition
        firstNameTextField.text = leadDetail.firstName
        lastNameTextField.text = leadDetail.lastName
        genderTextField.text = leadDetail.gender
        dobTextField.text = leadDetail.dob
        addressTextField.text = leadDetail.address
        cityTextField.text = leadDetail.city
        stateTextField.text = leadDetail.state
        zipCodeTextField.text = leadDetail.zip
        homeOwnerTextField.text = leadDetail.hOwnerBoolString
        
        v1YearTextField.text = leadDetail.year
        v1MakeTextField.text = leadDetail.make
        v1ModelTextField.text = leadDetail.model
        
        v2YearTextField.text = leadDetail.year2
        v2MakeTextField.text = leadDetail.make2
        v2ModelTextField.text = leadDetail.model2
        
        phoneNumberTextField.text = leadDetail.phoneNo
        emailTextField.text = leadDetail.email
        
        lengthCoverageTextField.text = leadDetail.continousCoverage
        maritalStatusTextField.text = leadDetail.maritalStatusBoolString
        followUpDateTextField.text = leadDetail.followUpXDate
        
        phoneNumberTextField.isEnabled = false
        phoneNumberTextField.alpha = 0.6
        
        stateTextField.isEnabled = false
        stateTextField.alpha = 0.6
    }
    
    @IBAction func followUpDateEditBegin(_ sender: Any) {
        DatePickerDialog().show("Follow Up Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy"
                        self.followUpDateTextField.text = formatter.string(from: dt)
                    }
        }
    }
    
    @IBAction func dobEditBegin(_ sender: Any) {
        DatePickerDialog().show("Date Of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy"
                        self.dobTextField.text = formatter.string(from: dt)
                    }
        }
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        let updatedLead = MLLeadDetails(id: leadDetail.id ?? 0,
                      firstName: firstNameTextField.text ?? "",
                      lastName: lastNameTextField.text ?? "",
                      address: addressTextField.text ?? "",
                      city: cityTextField.text ?? "",
                      state: stateTextField.text ?? "",
                      zip: zipCodeTextField.text ?? "",
                      phoneNo: phoneNumberTextField.text ?? "",
                      company: leadDetail.company ?? "",
                      make: v1MakeTextField.text ?? "",
                      make2: v2MakeTextField.text ?? "",
                      year: v1YearTextField.text ?? "",
                      year2: v2YearTextField.text ?? "",
                      model: v1ModelTextField.text ?? "",
                      model2: v2ModelTextField.text ?? "",
                      dob: dobTextField.text ?? "",
                      email: emailTextField.text ?? "",
                      continousCoverage: lengthCoverageTextField.text ?? "",
                      gender: genderTextField.text ?? "",
                      disposition: dispositionTextField.text ?? "",
                      hOwnerShip: homeOwnerTextField.text ?? "",
                      transferTo: leadDetail.transferTo ?? "",
                      isActive: leadDetail.isActive,
                      maritalStatus: maritalStatusTextField.text ?? "",
                      agencyOwner: leadDetail.agencyOwner ?? "",
                      agencyProducer: producerEmailTextField.text ?? "",
                      viciLeadID: leadDetail.viciLeadID ?? 0)
        
        
        updatedLead.comments = noteTextField.text ?? ""
        updatedLead.uId = AppUserDefaults.user.get()?.uID ?? 0
        
        updatedLead.dispositionId = dispositionList.filter({$0.name == updatedLead.disposition}).first?.id ?? 0
        updatedLead.agencyProducerId = producerList.filter({$0.name == updatedLead.agencyProducer}).first?.id ?? 0
        
        //Get lead Details
        let routerLeads = apiCategory.getLeadRouter(fromDetail: MLDetailAPIs.update(info: updatedLead))
        
        MLServiceManager.performRequest(router: routerLeads).crExecute { [weak self] (response: Int?) in
            
            print("Response is \(response ?? -1)")
            self?.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
