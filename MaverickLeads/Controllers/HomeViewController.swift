//
//  HomeViewController.swift
//  MaverickLeads
//
//  Created by Developer on 10/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var userRoleLabel: UILabel!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    var user: MLUser!
    var mode: HomeViewMode = .superAccess
    var selectedSuperAccess: UserSuperAccess = .liveLeads

    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(top: 50.0,
    left: 20.0,
    bottom: 50.0,
    right: 20.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userRoleLabel.text = mode == .superAccess ? user.uRole.displayName.uppercased() : selectedSuperAccess.displayText.uppercased()
        navigationItem.title = "Dashboard"
    }
 
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
      //2
      let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
      let availableWidth = view.frame.width - paddingSpace
      let widthPerItem = availableWidth / itemsPerRow
      
      return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
      return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if mode == .superAccess {
            let superAccess = user.getSuperAccess[indexPath.row]
            
            if superAccess == .logout {
                AppUserDefaults.user.set(value: nil)
                self.navigationController?.popViewController(animated: true)
                return
            }
            
            if user.getSubAccess[superAccess]?.count ?? 0 > 0 {
                push(viewController: HomeViewController.self, storyboard: AppConstants.Storyboards.leads) { [weak self] (vc) in
                    guard let self = self else {return}
                    vc.user = self.user
                    vc.mode = .subAccess
                    vc.selectedSuperAccess = superAccess
                }
            
            } else {
                var subAccess = UserSubAccess.none

                if user.uRole == .agencyOwner {
                    switch superAccess {
                    case .livePnp:
                        subAccess = .liveTransfers
                    case .pingAndPost:
                        subAccess = .pingAndPost
                    default:
                        break
                    }
                }
                
                let apiCategory = APICategory.getCatFrom(superAccess: superAccess, subAccess: subAccess)

                let pgController = PagerController(withTitle: superAccess.displayText.uppercased(), api_category: apiCategory, sub_access: .none)
                navigationController?.pushViewController(pgController, animated: true)
            }
          
        } else {
            guard let subAccess = user.getSubAccess[selectedSuperAccess]?[ indexPath.row] else {return}
            
            let apiCategory = APICategory.getCatFrom(superAccess: selectedSuperAccess, subAccess: subAccess)
            
            let pgController = PagerController(withTitle: subAccess.displayText.uppercased(), api_category: apiCategory, sub_access: subAccess)
            navigationController?.pushViewController(pgController, animated: true)
        }
    
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if mode == .superAccess {
            return user.getSuperAccess.count
        
        } else {
            return user.getSubAccess[selectedSuperAccess]?.count ?? 0
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as? AccessCollectionViewCell else {return UICollectionViewCell()}
        let superAccess = user.getSuperAccess[indexPath.row]
        
        if mode == .superAccess {
            cell.accessLabel.text = superAccess.displayText.uppercased()
            cell.accessImage.image = superAccess.icon
        
        } else {
            
            cell.accessLabel.text = user.getSubAccess[selectedSuperAccess]?[ indexPath.row].displayText.uppercased() ?? ""
            cell.accessImage.image = user.getSubAccess[selectedSuperAccess]?[ indexPath.row].icon ?? UIImage()
            
        }
        
        
        
        // Configure the cell
        return cell
    }

}

enum HomeViewMode {
    case superAccess, subAccess
}
