//
//  ExportViewController.swift
//  MaverickLeads
//
//  Created by Developer on 11/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit
import DatePickerDialog

class ExportViewController: AppViewController {
    @IBOutlet weak var fromDateView: UIView!
    @IBOutlet weak var toDateView: UIView!
    
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var getStatsButton: UIButton!
    @IBOutlet weak var tableView: UITableView!

    var apiCategory: APICategory!
    
    var disableDatePickerView: Bool = true {
        didSet {
            fromDateView.isHidden = disableDatePickerView
            toDateView.isHidden = disableDatePickerView
            getStatsButton.isHidden = disableDatePickerView
        }
    }
    
    var leads: [MLLead] {
        return AppUserDefaults.leads.get() ?? [MLLead]()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fromDate.inputView = UIView()
        toDate.inputView = UIView()
        
        disableDatePickerView = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
    }
    
    func updateUI() {
        tableView.reloadData()
    }
    
    @IBAction func getStatsButtonTapped(_ sender: Any) {
    }
    
    @IBAction func fromDateEditingBegin(_ sender: Any) {
           
        DatePickerDialog(buttonColor: UIColor.appMain).show("Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
               (date) -> Void in
               if let dt = date {
                           let formatter = DateFormatter()
                           formatter.dateFormat = "MM/dd/yyyy"
                           self.fromDate.text = formatter.string(from: dt)
                       }
           }
           
       }
       
       @IBAction func toDateEditingBegin(_ sender: Any) {
           
           DatePickerDialog().show("End Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
               (date) -> Void in
               if let dt = date {
                           let formatter = DateFormatter()
                           formatter.dateFormat = "MM/dd/yyyy"
                           self.toDate.text = formatter.string(from: dt)
                       }
           }
           
       }
}

extension ExportViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExportTableViewCell.self)) as? ExportTableViewCell else { return UITableViewCell()}
        
        cell.configureCell(lead: leads[indexPath.row]) { [weak self] cellAction in
            
            if cellAction == .phone {
                self?.leads[indexPath.row].phoneNo?.dialNumber()
            } else {
                let email = self?.leads[indexPath.row].email ?? ""
                
                self?.showEmailComposer(withEmail: email)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let lead = leads[indexPath.row  ]
        
        //Get lead Details
        let routerLeads = apiCategory.getDetailAPI(fromLead: lead)
        
        MLServiceManager.performRequest(router: routerLeads).crExecute { [weak self] (response: [MLLeadDetails]?) in
            
            guard let responseDetail = response?.first else {return}
            
            self?.push(viewController: EditableFormViewController.self, storyboard: AppConstants.Storyboards.leads) { vc in
                vc.leadDetail = responseDetail
                vc.apiCategory = self?.apiCategory
            }
        }
        
    }
}
