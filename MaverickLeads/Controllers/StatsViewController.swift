//
//  StatsViewController.swift
//  MaverickLeads
//
//  Created by Developer on 11/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit
import DatePickerDialog

enum TLWiseCount {
    case rejected, accepted, total
}

class StatsViewController: UIViewController {

    @IBOutlet weak var tlWiseStackView: UIStackView!
    @IBOutlet weak var dispositionStackView: UIStackView!
    @IBOutlet weak var tlWiseSegmentedControl: UISegmentedControl!
    @IBOutlet weak var dispositionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tlWiseLeads: UITextView!
    @IBOutlet weak var dispositionStatsTv: UITextView!
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    
    @IBOutlet weak var tlWiseTableView: UITableView!
    @IBOutlet weak var dispositionTableView: UITableView!
    @IBOutlet weak var totalCountLabel: UILabel!
    @IBOutlet weak var acceptedCountLabel: UILabel!
    @IBOutlet weak var rejectedCountLabel: UILabel!
    
    var accepted, rejected, total: Int!
    var dispositionDataSource: [String: [MLLead]]!
    var leadsAgentDataSource: [String: [MLLead]]!
    
    var tlLeadCountData: [TLWiseCount: [MLCountResponse]]!
    
    var apiCategory: APICategory!
    
    var leads: [MLLead] {
        return AppUserDefaults.leads.get() ?? [MLLead]()
    }
    
    var rejectedLeads: [MLLead] {
        leads.filter({$0.disposition?.isRejected ?? false})
    }
    
    var acceptedLeads: [MLLead] {
        leads.filter({($0.disposition?.isRejected ?? false) == false})
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fromDate.inputView = UIView()
        toDate.inputView = UIView()
        
        accepted = 0
        rejected = 0
        total = 0
        
        dispositionDataSource = [String: [MLLead]]()
        tlLeadCountData = [TLWiseCount: [MLCountResponse]]()
        leadsAgentDataSource = [String: [MLLead]]()
        
        tlLeadCountData[TLWiseCount.total] = [MLCountResponse]()
        tlLeadCountData[TLWiseCount.accepted] = [MLCountResponse]()
        tlLeadCountData[TLWiseCount.rejected] = [MLCountResponse]()
        
        dispositionTableView.tableFooterView = UIView()
        tlWiseTableView.tableFooterView = UIView()
        
        AppUserDefaults.leads.set(value: [MLLead]())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
        getAPIData()

        formatLeadsData()
    }
    
    func formatLeadsData() {
        dispositionDataSource.removeAll()
        leadsAgentDataSource.removeAll()
        
        for aLead in leads {
            
            // Disposition based formatting of data
            if dispositionDataSource[aLead.disposition ?? ""] == nil {
               dispositionDataSource[aLead.disposition ?? ""] = [MLLead]()
            }
            
            dispositionDataSource[aLead.disposition ?? ""]?.append(aLead)
            
            
            // Agency owner formatting of data
//            if tlLeadCountData[aLead.agency ?? ""] == nil {
//               tlLeadCountData[aLead.agency ?? ""] = [MLLead]()
//            }
//
//            tlLeadCountData[aLead.agency ?? ""]?.append(aLead)
//
            // Agent data formatting
            if leadsAgentDataSource[aLead.transferTo ?? ""] == nil {
               leadsAgentDataSource[aLead.transferTo ?? ""] = [MLLead]()
            }
            
            leadsAgentDataSource[aLead.transferTo ?? ""]?.append(aLead)
        }
        
        updateUI()
    }
    
    func updateUI() {
        acceptedCountLabel.text = String(acceptedLeads.count)
        rejectedCountLabel.text = String(rejectedLeads.count)
        totalCountLabel.text = String(acceptedLeads.count + rejectedLeads.count)
        
        if apiCategory == APICategory.liveAgent ||
            apiCategory == APICategory.livePnpAgent ||
            apiCategory == APICategory.pnpAgent {
            tlWiseStackView.isHidden = false
            dispositionStackView.isHidden = true
//            updateAgentStats()
        }
//
//        updateDispositionStats()
//        updateTLWiseStats()
        
        dispositionTableView.reloadData()
        tlWiseTableView.reloadData()
    }

    
    func getRowCountForTable(tableView: UITableView) -> Int {
        
        if tableView == dispositionTableView {
            let rejectedState = dispositionSegmentedControl.selectedSegmentIndex == 0
            let requiredKeys = dispositionDataSource.keys.filter({$0.isRejected == rejectedState})
            
            return requiredKeys.count
        } else {
            switch tlWiseSegmentedControl.selectedSegmentIndex {
            case 0:
                return tlLeadCountData[.rejected]!.count
            case 1:
                return tlLeadCountData[.accepted]!.count
            case 2:
                return tlLeadCountData[.total]!.count
            default:
                return 0
            }
        }
        
    }
    
    func getDataForTable(tableView: UITableView, indexPath: IndexPath) -> (leftLabel: String, rightLabel: String) {
        
        if tableView == dispositionTableView {
            let rejectedState = dispositionSegmentedControl.selectedSegmentIndex == 0
            let requiredKeys = dispositionDataSource.keys.filter({$0.isRejected == rejectedState})
            let key = requiredKeys[indexPath.row]
            
            return ("\(key)", "Total: \(dispositionDataSource[key]?.count ?? 0)")
        } else {
            switch tlWiseSegmentedControl.selectedSegmentIndex {
            case 0:
                let leftLabel = tlLeadCountData[.rejected]![indexPath.row].name ?? ""
                let rightLabel = "Total: \(tlLeadCountData[.rejected]![indexPath.row].total)"
                return (leftLabel, rightLabel)
            case 1:
                let leftLabel = (tlLeadCountData[.accepted]![indexPath.row].name ?? "")
                let rightLabel = "Total: \(tlLeadCountData[.accepted]![indexPath.row].total )"
                return (leftLabel, rightLabel)
            case 2:
                let leftLabel = (tlLeadCountData[.total]![indexPath.row].name ?? "")
                let rightLabel = "Total: \(tlLeadCountData[.total]![indexPath.row].total )"
                return (leftLabel, rightLabel)
            default:
                return ("", "")
            }
        }
        
    }
    
    func updateAgentStats() {
        var agencyOwnerWise = ""
        
        for aDictKey in leadsAgentDataSource.keys {
            var requiredCount = 0 //tlLeadCountData[aDictKey]?.count ?? 0
            
            switch tlWiseSegmentedControl.selectedSegmentIndex {
            case 0:
                requiredCount = leadsAgentDataSource[aDictKey]?.filter({$0.disposition?.isRejected ?? false}).count ?? 0
            case 1:
                requiredCount = leadsAgentDataSource[aDictKey]?.filter({($0.disposition?.isRejected ?? false) == false}).count ?? 0
                
            case 2:
                requiredCount = leadsAgentDataSource[aDictKey]?.count ?? 0
            
            default:
                requiredCount = 0
            }
            
            let text = "\(aDictKey.paddedToWidth(40)) Total:\(requiredCount)\n"
//            let text = "\(aDictKey) (\(requiredCount))\n"
            agencyOwnerWise = agencyOwnerWise + text
        }
        
        tlWiseLeads.text = agencyOwnerWise
    }
    
    func getAPIData() {
        
        //Get leads
        let queryLeads = MLTeamLeadQuery(from: fromDate.text, to: toDate.text)
        let routerLeads = LeadRouter.general(category: apiCategory, api: .leads(query: queryLeads))
        
        MLServiceManager.performRequest(router: routerLeads).crExecute { [weak self] (response: [MLLead]?) in
            
            if let leadsRetrieved = response {
                AppUserDefaults.leads.set(value: leadsRetrieved)

                self?.updateUI()
                self?.formatLeadsData()
            }
        }
        
        //Get dispositions
        let disRouter = LeadRouter.dipositionList
            
        MLServiceManager.performRequest(router: disRouter).crExecute { (response: [MLDisposition]?) in
            AppUserDefaults.dispositionList.set(value: response ?? [MLDisposition]())
        }
        
        let user = AppUserDefaults.user.get()!
        var agencyOwnerId = 0
        var producerId = 0
        
        if user.uRole == .agencyOwner {
            agencyOwnerId = user.uID
        } else if user.uRole == .agencyProducer {
            producerId = user.uID
        }
        
        let agencyProducer = LeadRouter.agencyProducer(agencyOwnerId: agencyOwnerId, producerId: producerId)
            
        MLServiceManager.performRequest(router: agencyProducer).crExecute { (response: [MLProducer]?) in
                AppUserDefaults.producerList.set(value: response ?? [MLProducer]())
        }
        
        if apiCategory == APICategory.liveTl ||
            apiCategory == APICategory.pnpTl ||
            apiCategory == APICategory.livePnpTl ||
            apiCategory == APICategory.pnpAgent ||
            apiCategory == APICategory.liveAgent ||
            apiCategory == APICategory.livePnpAgent {
                
            tlWiseStackView.isHidden = false
            
            //Get tl count total
            let queryLeads = MLTeamLeadQuery(from: fromDate.text, to: toDate.text)
            var leadWiseCount: LeadRouter!
            
            if apiCategory == APICategory.liveTl ||
                apiCategory == APICategory.pnpTl ||
                    apiCategory == APICategory.livePnpTl {
                leadWiseCount = LeadRouter.general(category: apiCategory, api: .leadWise(query: queryLeads))
            } else {
                leadWiseCount = LeadRouter.general(category: apiCategory, api: .totalSummary(query: queryLeads))
            }
            
            MLServiceManager.performRequest(router: leadWiseCount).crExecute { [weak self] (response: [MLCountResponse]?) in
                
                if let tlWiseCountData = response {
                    self?.tlLeadCountData[TLWiseCount.total] = tlWiseCountData
                }
                
                self?.updateUI()
            }
            
            //Get tl reject total
            var leadWiseRejected: LeadRouter!
            if apiCategory == APICategory.liveTl ||
                apiCategory == APICategory.pnpTl ||
                    apiCategory == APICategory.livePnpTl {
                leadWiseRejected = LeadRouter.general(category: apiCategory, api: .leadWiseRejected(query: queryLeads))
            } else {
                leadWiseRejected = LeadRouter.general(category: apiCategory, api: .rejectedSummary(query: queryLeads))
            }
                
                
            
            MLServiceManager.performRequest(router: leadWiseRejected).crExecute { [weak self] (response: [MLCountResponse]?) in
                
                if let tlWiseCountData = response {
                    self?.tlLeadCountData[TLWiseCount.rejected] = tlWiseCountData
                }
                
                self?.updateUI()
            }
            
            //Get tl reject total
            var leadWiseAccepted: LeadRouter!
            if apiCategory == APICategory.liveTl ||
                apiCategory == APICategory.pnpTl ||
                    apiCategory == APICategory.livePnpTl {
                leadWiseAccepted = LeadRouter.general(category: apiCategory, api: .leadWiseAccepted(query: queryLeads))
            } else {
                leadWiseAccepted = LeadRouter.general(category: apiCategory, api: .acceptedSummary(query: queryLeads))
            }
            
            MLServiceManager.performRequest(router: leadWiseAccepted).crExecute { [weak self] (response: [MLCountResponse]?) in
                
                if let tlWiseCountData = response {
                    self?.tlLeadCountData[TLWiseCount.accepted] = tlWiseCountData
                }
                
                self?.updateUI()
            }
            
            
        } else {
            tlWiseStackView.isHidden = true
        }
    
    }
    
    
    @IBAction func tlSegmentedValueChanged(_ sender: Any) {
        tlWiseTableView.reloadData()
    }
    
    @IBAction func segmentedControlChanged(_ sender: Any) {
        dispositionTableView.reloadData()
    }
    
    @IBAction func getStatsButtonTapped(_ sender: Any) {
        getAPIData()
    }

    @IBAction func fromDateEditingBegin(_ sender: Any) {
        
        DatePickerDialog().show("Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy"
                        self.fromDate.text = formatter.string(from: dt)
                    }
        }
        
    }
    
    @IBAction func toDateEditingBegin(_ sender: Any) {
        
        DatePickerDialog().show("End Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy"
                        self.toDate.text = formatter.string(from: dt)
                    }
        }
        
    }
    
}


extension StatsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getRowCountForTable(tableView: tableView)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StatsCountTableViewCell.self)) as? StatsCountTableViewCell else {return UITableViewCell()}
        let cellData = getDataForTable(tableView: tableView, indexPath: indexPath)
        
        cell.leftLabel.text = cellData.leftLabel
        cell.rightLabel.text = cellData.rightLabel
        
        return cell
    }
}
