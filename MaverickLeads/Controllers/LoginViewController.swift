//
//  LoginViewController.swift
//  MaverickLeads
//
//  Created by Developer on 06/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit
import SCLAlertView

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var checkmarkButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    @IBAction func checkmarkTapped(_ sender: Any) {
        checkmarkButton.isSelected = !checkmarkButton.isSelected
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        
        if userNameTextField.text!.isEmpty || passwordTextField.text!.isEmpty {
            showLoginError()
            return
        }

        if checkmarkButton.isSelected {
            UserDefaults.standard.set(userNameTextField.text, forKey: "username")
            UserDefaults.standard.set(passwordTextField.text, forKey: "password")
        } else {
            UserDefaults.standard.set(nil, forKey: "username")
            UserDefaults.standard.set(nil, forKey: "password")
        }
        
        
        let router = UserRouter.login(username: userNameTextField.text ?? "", password: passwordTextField.text ?? "")
        
        MLServiceManager.performRequest(router: router).crExecute { [weak self] (mUsers: [MLUser]?) in
            if let users = mUsers, users.count != 0 {
                let user = users[0];
                guard let self = self else {return}
                
                print("Response: \(user.uRole.displayName)")
                
                if user.isLogged {
                    print("Logged in successfully")

                    // Set user session
                    AppUserDefaults.user.set(value: user)
                    
                    // Update push token
                    self.updatePushToken()
                    
                    self.push(viewController: HomeViewController.self, storyboard: AppConstants.Storyboards.leads) { vc in
                        vc.user = user
                    }
                    
                } else {
                    self.showLoginError()
                }
            }
        }
    }
    
    func showLoginError() {
        if userNameTextField.text!.isEmpty {
            SCLAlertView().showInfo("Username Empty", subTitle: "Please enter username to proceed") // Info
        } else if passwordTextField.text!.isEmpty {
            SCLAlertView().showInfo("Password Empty", subTitle: "Please enter password to proceed") // Info
        } else {
            SCLAlertView().showError("Invalid Credentials", subTitle: "Please check username and password entered correctly.")
        }
    }
    
    func setupView() {
        navigationItem.title = "Maverick Leads"
        
        userNameTextField.setLeftPaddingPoints(16)
        passwordTextField.setLeftPaddingPoints(16)
        
        if let username = UserDefaults.standard.string(forKey: "username") {
            userNameTextField.text = username
            checkmarkButton.isSelected = true
        }
        
        if let password = UserDefaults.standard.string(forKey: "password") {
            passwordTextField.text = password
            checkmarkButton.isSelected = true
        }
        
//        containerView.applyeShadowWithCornerRadius()

    }
    
    func updatePushToken() {
        guard let user = AppUserDefaults.user.get() else {return}
        guard let pushToken = AppUserDefaults.pushToken.get() else {return}
        
        if user.uRole == .agencyOwner || user.uRole == .agencyProducer {
            if pushToken.isEmpty {return}
                
                let router = UserRouter.updatePushToken(userId: user.uID, userRole: user.role ?? "", token: pushToken)
                
                MLServiceManager.performRequest(router: router).crExecute { (response: Int?) in
                    print("Response is \(response ?? 0)")
                }
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
