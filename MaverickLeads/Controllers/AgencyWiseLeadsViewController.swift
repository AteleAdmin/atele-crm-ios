//
//  AgencyWiseLeadsViewController.swift
//  MaverickLeads
//
//  Created by Developer on 14/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import UIKit

class AgencyWiseLeadsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var leads: [MLLead] {
        return AppUserDefaults.leads.get() ?? [MLLead]()
    }
    
    var dataSource: [String: [MLLead]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataSource.removeAll()
        
        for aLead in leads {
            
            if dataSource[aLead.agency ?? ""] == nil {
               dataSource[aLead.agency ?? ""] = [MLLead]()
            }
            
            dataSource[aLead.agency ?? ""]?.append(aLead)
        }
        
        tableView.reloadData()
    }
    
    func setupView() {
        dataSource = [String: [MLLead]]()
        tableView.tableFooterView = UIView()
    }

}

extension AgencyWiseLeadsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.keys.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgencyLeadsTableViewCell.self)) as? AgencyLeadsTableViewCell else { return UITableViewCell()}
        
        var index = indexPath.row - 1
        
        if index < 0 {
            index = 0
        }

        if index > dataSource.keys.count - 1 {
            return cell
        }
        
        let key = Array(dataSource.keys)[index]
        cell.configureCell(forIndexPath: indexPath, source: dataSource[key]!)
        
        return cell
    }
}
