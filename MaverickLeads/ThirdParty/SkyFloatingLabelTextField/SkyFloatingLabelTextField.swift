//  Copyright 2016 Skyscanner Ltd
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//See the License for the specific language governing permissions and limitations under the License.

import UIKit

/**
 A beautiful and flexible textfield implementation with support for title label, error message and placeholder.
 */
//@IBDesignable
open class SkyFloatingLabelTextField: UITextField {
    /// A Boolean value that determines if the language displayed is LTR. Default value set automatically from the application language settings.
    var isLTRLanguage = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight {
        didSet {
           self.updateTextAligment()
        }
    }
    @IBInspectable
    var startingImage: UIImage? {
        didSet {
            updateControl()
        }
    }
    
    var prevText: String? = nil
    
    func setPreviousText(flag: Bool) {
        prevText = flag ? text : nil
    }
    
    func isTextChanged(showError: Bool = false) -> Bool {
        let textChanged = (text != prevText)
        if prevText != nil && textChanged == false {
            hasErrorMessage = showError
        }
        return textChanged
    }
    
    func setImage() {
        if startingImageView == nil && startingImage != nil {
            startingImageView = UIImageView(frame: CGRect(x: 10, y: bounds.size.height - boxHeight, width: imageWidth - 10, height: boxHeight))
            startingImageView?.contentMode = .scaleAspectFit
            addSubview(startingImageView!)
        }
        startingImageView?.frame = CGRect(x: 10, y: bounds.size.height - boxHeight, width: imageWidth - 10, height: boxHeight)
        startingImageView?.image = startingImage
    }
    private var startingImageView: UIImageView?
    
    var isDisabled: Bool = false {
        didSet {
//            if isDisabled {
//                textColor = UIColor.darkGray
//                titleColor = UIColor.darkGray
//            } else {
//                textColor = UIColor.textColor
//                titleColor = UIColor.zenDarkGray
//            }
            textColor = UIColor.darkGray
            titleColor = UIColor.darkGray
            updateTitleColor()
        }
    }
    
    fileprivate func updateTextAligment() {
        if(self.isLTRLanguage) {
            self.textAlignment = .left
        } else {
            self.textAlignment = .right
        }
    }

    // MARK: Animation timing

    /// The value of the title appearing duration
    open var titleFadeInDuration: TimeInterval = 0.2
    /// The value of the title disappearing duration
    open var titleFadeOutDuration: TimeInterval = 0.3

    // MARK: Colors

    fileprivate var cachedTextColor: UIColor?

    /// A UIColor value that determines the text color of the editable text
    /*@IBInspectable
    override open var textColor:UIColor? {
        set {
            self.cachedTextColor = newValue
            self.updateControl(false)
        }
        get {
            return cachedTextColor
        }
    }*/

    /// A UIColor value that determines text color of the placeholder label
    /*@IBInspectable
    open var placeholderColor:UIColor = UIColor.lightGray {
        didSet {
            self.updatePlaceholder()
        }
    }*/
    //Ab Added
    open var placeholderColor: UIColor = UIColor(red: 129/244, green: 131/255, blue: 132/255, alpha: 1.0)
    /// A UIColor value that determines text color of the placeholder label
    /*
     AB commented
     @IBInspectable open var placeholderFont:UIFont? {
        didSet {
            self.updatePlaceholder()
        }
    }*/

    var placeholderFont: UIFont = UIFont.appFront(ofSize: 12)
    
    fileprivate func updatePlaceholder() {
        if let placeholder = self.placeholder {
            self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor,
                                                                                              NSAttributedString.Key.font: placeholderFont])
        }
    }

    /// A UIColor value that determines the text color of the title label when in the normal state
    /*@IBInspectable
    open var titleColor:UIColor = UIColor.gray {
        didSet {
            self.updateTitleColor()
        }
    }*/
    //Ab added
    open var titleColor: UIColor = UIColor.darkGray
    
    /// A UIColor value that determines the color of the bottom line when in the normal state
    /*@IBInspectable
    open var lineColor:UIColor = UIColor.lightGray {
        didSet {
            self.updateLineView()
        }
    }*/
    let lineColor: UIColor = UIColor(red: 171/244, green: 172/255, blue: 173/255, alpha: 1.0)
    /// A UIColor value that determines the color used for the title label and the line when the error message is not `nil`
    /*@IBInspectable
    open var errorColor:UIColor = UIColor.red {
        didSet {
            self.updateColors()
        }
    }*/
    //AB Added
    public let errorColor: UIColor = UIColor.red
    
    /// A UIColor value that determines the text color of the title label when editing
    /*@IBInspectable
    open var selectedTitleColor:UIColor = UIColor.blue {
        didSet {
            self.updateTitleColor()
        }
    }*/
    //AB Added
    public let selectedTitleColor: UIColor = UIColor.blue

    /// A UIColor value that determines the color of the line in a selected state
    /*@IBInspectable
    open var selectedLineColor:UIColor = UIColor.black {
        didSet {
            self.updateLineView()
        }
    }*/
    open var selectedLineColor: UIColor = UIColor.blue

    //AB Added
    /*@IBInspectable
    open var filledLineColor:UIColor = UIColor.black {
        didSet {
            self.updateLineView()
        }
    }*/
    //AB Added
    open var filledLineColor: UIColor = UIColor(red: 171/244, green: 172/255, blue: 173/255, alpha: 1.0)
    // MARK: Line height

    /// A CGFloat value that determines the height for the bottom line when the control is in the normal state
    /*@IBInspectable
    open var lineHeight:CGFloat = 0.5 {
        didSet {
            self.updateLineView()
            self.setNeedsDisplay()
        }
    }*/
    
    //Abdul Baseer Added
    let boxHeight: CGFloat = 44
    
    //AB Added
    //open let lineHeight: CGFloat = 44
    /// A CGFloat value that determines the height for the bottom line when the control is in a selected state
    /*@IBInspectable
    open var selectedLineHeight:CGFloat = 1.0 {
        didSet {
            self.updateLineView()
            self.setNeedsDisplay()
        }
    }*/
    //AB Added
    //open let selectedLineHeight: CGFloat = 44

    //AB Added
    @IBInspectable
    open var errorIcon: UIImage? = nil {
        didSet {
            self.updateControl()
            setNeedsDisplay()
        }
    }
    
    //AB Added
    @IBInspectable
    open var titleLabelHide: Bool = true {
        didSet {
            self.updateControl()
            setNeedsDisplay()
        }
    }
    
    //open let errorIcon:UIImage = #imageLiteral(resourceName: "field_cross_icon")
    
    @IBInspectable open var isPassword: Bool = false {
        didSet {
            self.isSecureTextEntry = isPassword
            self.updateControl()
            setNeedsDisplay()
        }
    }
    // MARK: View components

    /// The internal `UIView` to display the line below the text input.
    //open var lineView: UIView!
    
    //Abdul Baseer Added
    open var boxView: UIView!
    
    /// The internal `UILabel` that displays the selected, deselected title or the error message based on the current state.
    open var titleLabel: UILabel!

    //AB Added
    open var errorIconView: UIImageView!
    //AB Added 
    open var passwordButton: UIButton!
    // MARK: Properties

    /**
    The formatter to use before displaying content in the title label. This can be the `title`, `selectedTitle` 
     //or the `errorMessage`.
    The default implementation converts the text to uppercase.
    */
    open var titleFormatter:((String) -> String) = { (text:String) -> String in
        return text
        //return text.uppercased()
    }

    /**
     Identifies whether the text object should hide the text being entered.
     */
    override open var isSecureTextEntry:Bool {
        set {
            super.isSecureTextEntry = newValue
            self.fixCaretPosition()
        }
        get {
            return super.isSecureTextEntry
        }
    }

    /// A String value for the error message to display.
    /*open var errorMessage:String? {
        didSet {
            self.updateControl(true)
        }
    }*/
    
    
    open var hasErrorMessage: Bool = false {
        didSet {
            tintColor = (hasErrorMessage) ? errorColor : .lightGray
            self.updateColors()
            layer.cornerRadius = 5
            layer.masksToBounds = true
            //self.updateControl(true)
        }
    }
    
    func shouldShowError() -> Bool {
        return isSelected && hasErrorMessage
    }

    /// The backing property for the highlighted property
    fileprivate var _highlighted = false

    /// A Boolean value that determines whether the receiver is highlighted. When changing this value, highlighting will be done with animation
    override open var isHighlighted:Bool {
        get {
            return _highlighted
        }
        set {
            _highlighted = newValue
            self.updateTitleColor()
            self.updateBoxView()
        }
    }

    /// A Boolean value that determines whether the textfield is being edited or is selected.
    open var editingOrSelected:Bool {
        get {
            return super.isEditing || self.isSelected;
        }
    }

    //AB Added 
    override open var isEnabled: Bool {
        didSet {
            self.updateControl(true)
        }
    }
    
    
    //AB Added
    open var isFilled:Bool {
        get {
            return (self.text?.count ?? 0) > 0
        }
    }
    
    /// A Boolean value that determines whether the receiver has an error message.
    /*open var hasErrorMessage:Bool {
        get {
            return self.errorMessage != nil && self.errorMessage != ""
        }
    }*/

    //AB Added
    open var hasErrorIcon:Bool = false
    
    fileprivate var _renderingInInterfaceBuilder:Bool = false

    /// The text content of the textfield
    //@IBInspectable
    override open var text:String? {
        didSet {
            self.updateControl(false)
        }
    }
    /**
     The String to display when the input field is empty.
     The placeholder can also appear in the title label when both `title` `selectedTitle` and are `nil`.
     */
    @IBInspectable
    override open var placeholder:String? {
        didSet {
            self.setNeedsDisplay()
            self.updatePlaceholder()
            self.updateTitleLabel()
        }
    }

    /// The String to display when the textfield is editing and the input is not empty.
    /*@IBInspectable open var selectedTitle:String? {
        didSet {
            self.updateControl()
        }
    }
*/
    /// The String to display when the textfield is not editing and the input is not empty.
    @IBInspectable open var title:String? {
        didSet {
            self.updateControl()
        }
    }

    // Determines whether the field is selected. When selected, the title floats above the textbox.
    open override var isSelected:Bool {
        didSet {
            self.updateControl(true)
        }
    }

    // MARK: - Initializers

    /**
    Initializes the control
    - parameter frame the frame of the control
    */
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.init_SkyFloatingLabelTextField()
    }

    /**
     Intialzies the control by deserializing it
     - parameter coder the object to deserialize the control from
     */
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.init_SkyFloatingLabelTextField()
    }

    fileprivate final func init_SkyFloatingLabelTextField() {
        self.borderStyle = .none
        self.createTitleLabel()
        self.createBoxView()
        self.updateColors()
        self.addEditingChangedObserver()
        self.addEditingBeginObserver()
        self.addEditingEndObserver()
        self.updateTextAligment()
        self.settingsForFR()
    }
    
    func settingsForFR() {
        
        // color Settings
        self.textColor = UIColor.gray
        self.font = UIFont.appFront(ofSize: 12)
        self.tintColor = .lightGray
    }
    
    fileprivate func addEditingChangedObserver() {
        self.addTarget(self, action: #selector(SkyFloatingLabelTextField.editingChanged), for: .editingChanged)
    }
    fileprivate func addEditingBeginObserver() {
        self.addTarget(self, action: #selector(SkyFloatingLabelTextField.editingBegin), for: .editingDidBegin)
    }
    fileprivate func addEditingEndObserver() {
        self.addTarget(self, action: #selector(SkyFloatingLabelTextField.editingEnd), for: .editingDidEnd)
    }

    /**
     Invoked when the editing state of the textfield changes. Override to respond to this change.
     */
    @objc open func editingChanged() {
        updateControl(true)
        updateTitleLabel(true)
    }
    
    @objc open func editingBegin() {
        placeholderColor = UIColor.white
        updatePlaceholder()
        isSelected = true
    }
    @objc open func editingEnd() {
        placeholderColor = UIColor.gray
        updatePlaceholder()
        isSelected = false
    }

    // MARK: create components

    fileprivate func createTitleLabel() {
        let titleLabel = UILabel()
        titleLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        titleLabel.font = UIFont.appFront(ofSize: 9)
        titleLabel.alpha = 0.0
        titleLabel.textColor = self.titleColor
        self.addSubview(titleLabel)
        self.titleLabel = titleLabel
    }
/*
    fileprivate func createLineView() {
        
        if self.lineView == nil {
            let lineView = UIView()
            lineView.isUserInteractionEnabled = false
            lineView.layer.cornerRadius = 5
            lineView.layer.borderWidth = 1
            self.lineView = lineView
            
            //self.configureDefaultLineHeight()
        }
        //lineView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        self.addSubview(lineView)
        self.sendSubview(toBack: lineView)
    }
*/
    
    //Abdul Baseer Added
    fileprivate func createBoxView() {
        
        if self.boxView == nil {
            let boxView = UIView()
            boxView.isUserInteractionEnabled = false
            boxView.backgroundColor = UIColor.white
            boxView.layer.cornerRadius = 5
            boxView.layer.borderWidth = 1
            self.boxView = boxView
            
            //self.configureDefaultLineHeight()
        }
        //lineView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        self.addSubview(boxView)
        self.sendSubviewToBack(boxView)
    }
    
/*
    fileprivate func configureDefaultLineHeight() {
        let onePixel:CGFloat = 1.0 / UIScreen.main.scale
        self.lineHeight = 2.0 * onePixel
        self.selectedLineHeight = 2.0 * self.lineHeight
    }
*/
    //AB Added
    fileprivate func addErrorIcon() {
        removeErrorIcon()
        let rect = CGRect(x: self.frame.width-30, y: 6, width: 22, height: self.frame.height)
        let errorButton = UIButton(frame: rect)
        errorButton.setImage(self.errorIcon, for: .normal)
        errorButton.contentMode = .scaleAspectFit
        errorButton.addTarget(self, action: #selector(self.clearText), for: .touchDown)
        errorButton.tag = 1122
        self.addSubview(errorButton)
        self.hasErrorIcon = true
    }
    @objc func clearText() {
        self.text = ""
        self.hasErrorMessage = false
    }

    //AB Added
    fileprivate func removeErrorIcon() {
        for view in self.subviews {
            if view.tag == 1122 {
                view.removeFromSuperview()
            }
        }
        self.hasErrorIcon = false
    }
    
    //AB Added
    func addPasswordButton() {
        let title = removePasswordButton() ?? (isSecureTextEntry ? "SHOW" : "HIDE")
        if (text ?? "").count > 0 {
            let rect = CGRect(x: (hasErrorMessage && errorIcon != nil) ? (frame.width - 90):(frame.width - 60), y: 20, width: 60, height: frame.height - 20)
            passwordButton = UIButton(frame: rect)
            passwordButton.setTitleColor(UIColor.blue, for: .normal)
            passwordButton.setTitle(title, for: .normal)
            passwordButton.titleLabel?.font = UIFont.appFront(ofSize: 12, type: .bold)
            passwordButton.tag = 90553046
            passwordButton.addTarget(self, action: #selector(showHidePassword(_:)), for: .touchDown)
            addSubview(passwordButton)
        }
    }
    
    func removePasswordButton() -> String? {
        if passwordButton != nil {
            let title = passwordButton.currentTitle
            passwordButton.removeFromSuperview()
            passwordButton = nil
            return title
        }
        return nil
    }
    
    @objc func showHidePassword(_ sender: UIButton) {
        print("password show hide clicked.")
        if sender.currentTitle == "SHOW" {
            self.isSecureTextEntry = false
            sender.setTitle("HIDE", for: .normal)
        }
        else {
            
            self.isSecureTextEntry = true
            sender.setTitle("SHOW", for: .normal)
        }
    }
    // MARK: Responder handling

    /**
     Attempt the control to become the first responder
     - returns: True when successfull becoming the first responder
    */
    @discardableResult
    override open func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        self.updateControl(true)
        return result
    }

    /**
     Attempt the control to resign being the first responder
     - returns: True when successfull resigning being the first responder
     */
    @discardableResult
    override open func resignFirstResponder() -> Bool {
        let result =  super.resignFirstResponder()
        self.updateControl(true)
        return result
    }

    // MARK: - View updates

    fileprivate func updateControl(_ animated:Bool = false) {
        setImage()
        self.updateErrorIcon()
        if self.isPassword {
            self.addPasswordButton()
        }
        self.updateColors()
        //self.updateLineView()
        self.updateBoxView()
        self.updateTitleLabel(animated)
    }

    fileprivate func updateBoxView() {
        if let boxView = self.boxView {
            boxView.frame = self.lineViewRectForBounds(self.bounds, editing: self.editingOrSelected)
        }
        //self.updateLineColor()
        self.updateBoxColor()
    }

/*    fileprivate func updateLineView() {
        if let lineView = self.lineView {
            lineView.frame = self.lineViewRectForBounds(self.bounds, editing: self.editingOrSelected)
        }
        self.updateLineColor()
    }
*/
    // MARK: - Color updates

    /// Update the colors for the control. Override to customize colors.
    open func updateColors() {
        //self.updateLineColor()
        self.updateBoxColor()
        self.updateTitleColor()
        self.updateTextColor()
    }

/*    fileprivate func updateLineColor() {
        if self.hasErrorMessage {
            self.lineView.backgroundColor = self.errorColor
        } else
            if isSelected { //AB Added
                self.lineView.backgroundColor = self.selectedLineColor
            }
            else if self.isFilled || isEnabled == false { //AB Added
                self.lineView.backgroundColor = self.filledLineColor
            }
            else {
                //AB commented //self.lineView.backgroundColor = self.editingOrSelected ? self.selectedLineColor : self.lineColor
                //AB Added
                self.lineView.backgroundColor = self.lineColor
        }
    }
*/
    fileprivate func updateBoxColor() {
        if self.hasErrorMessage {
            boxView.layer.borderColor = self.errorColor.cgColor
        } else
            if isSelected {
                boxView.layer.borderColor = self.selectedLineColor.cgColor
            }
            else if self.isFilled || isEnabled == false { //AB Added
                boxView.layer.borderColor = self.filledLineColor.cgColor
            }
            else {
                boxView.layer.borderColor = self.lineColor.cgColor
        }
    }

    fileprivate func updateErrorIcon() {
        if self.hasErrorMessage {
            self.addErrorIcon()
        }
        else {
            self.removeErrorIcon()
        }
    }
    
    
    fileprivate func updateTitleColor() {
        if self.hasErrorMessage {
            self.titleLabel.textColor = self.errorColor
        } else {
            if self.editingOrSelected || self.isHighlighted {
                self.titleLabel.textColor = self.selectedTitleColor
            } else {
                self.titleLabel.textColor = self.titleColor
            }
        }
    }

    fileprivate func updateTextColor() {
        if self.hasErrorMessage {
            super.textColor = self.cachedTextColor//self.errorColor //AB editing
        } else {
            super.textColor = self.cachedTextColor
        }
    }

    // MARK: - Title handling

    fileprivate func updateTitleLabel(_ animated:Bool = false) {

        var titleText:String? = nil
        if self.hasErrorMessage {
            titleText = self.titleOrPlaceholder()//self.titleFormatter(errorMessage!)
            
        } else {
            if self.editingOrSelected {
                //titleText = self.selectedTitleOrTitlePlaceholder()
                if titleText == nil {
                    titleText = self.titleOrPlaceholder()
                }
            } else {
                titleText = self.titleOrPlaceholder()
            }
        }

        /*if (titleText ?? "") == "Password" && self.hasErrorMessage {
            let aditionalText = " (Min 8 characters including 1 number or special character)"
            self.titleLabel.attributedText = (titleText!+aditionalText).add(font: UIFont.zenLight(ofSize: 9), specialString: aditionalText)
            
        } else {
        
            self.titleLabel.text = titleText
        }*/
        self.titleLabel.text = titleText
        self.updateTitleVisibility(animated)
    }
   

    fileprivate var _titleVisible = false

    /*
    *   Set this value to make the title visible
    */
    open func setTitleVisible(_ titleVisible:Bool, animated:Bool = false, animationCompletion: ((_ completed: Bool) -> Void)? = nil) {
        if(_titleVisible == titleVisible) {
            return
        }
        _titleVisible = titleVisible
        self.updateTitleColor()
        self.updateTitleVisibility(animated, completion: animationCompletion)
    }

    /**
     Returns whether the title is being displayed on the control.
     - returns: True if the title is displayed on the control, false otherwise.
     */
    open func isTitleVisible() -> Bool {
        return self.hasText || self.hasErrorMessage || _titleVisible || (isEditing) || !titleLabelHide
    }

    fileprivate func updateTitleVisibility(_ animated:Bool = false, completion: ((_ completed: Bool) -> Void)? = nil) {
        let alpha:CGFloat = self.isTitleVisible() ? 1.0 : 0.0
        let frame:CGRect = self.titleLabelRectForBounds(self.bounds, editing: self.isTitleVisible())
        let updateBlock = { () -> Void in
            self.titleLabel.alpha = alpha
            self.titleLabel.frame = frame
        }
        if animated {
            let animationOptions:UIView.AnimationOptions = .curveEaseOut;
            let duration = self.isTitleVisible() ? titleFadeInDuration : titleFadeOutDuration

            UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { () -> Void in
                updateBlock()
                }, completion: completion)
        } else {
            updateBlock()
            completion?(true)
        }
    }

    // MARK: - UITextField text/placeholder positioning overrides

    
    func getRect(for bounds: CGRect) -> CGRect {
        let titleHeight = self.titleHeight()
        //let lineHeight = self.selectedLineHeight
        var width = bounds.size.width
        var x: CGFloat = 10
        if startingImage != nil {
            x += imageWidth
            width -= imageWidth
        }
        if isPassword && passwordButton != nil {
            width -= passwordButton.frame.width
        }
        width -= 20
        
        return CGRect(x: x, y: -2+bounds.size.height + titleHeight - boxHeight, width: width, height: boxHeight+12)
    }
    
    /**
    Calculate the rectangle for the textfield when it is not being edited
    - parameter bounds: The current bounds of the field
    - returns: The rectangle that the textfield should render in
    */
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return getRect(for: bounds)
    }

    /**
     Calculate the rectangle for the textfield when it is being edited
     - parameter bounds: The current bounds of the field
     - returns: The rectangle that the textfield should render in
     */
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        /*super.editingRect(forBounds: bounds)
        let titleHeight = self.titleHeight()
        //let lineHeight = self.selectedLineHeight
        //asaa
        var width = bounds.size.width
        var x: CGFloat = 10
        if startingImage != nil {
            x += imageWidth
            width -= imageWidth
        }
        if isPassword && passwordButton != nil {
            width -= passwordButton.frame.width
        }
        if hasErrorMessage {
            width -= 30
        }
        
        //let rect = CGRect(x: 0, y: titleHeight, width: width, height: bounds.size.height - titleHeight - lineHeight)
        let rect = CGRect(x: x, y: 1+bounds.size.height + titleHeight - boxHeight , width: width, height: font?.lineHeight ?? boxHeight+12)
        return rect*/
        return getRect(for: bounds)
    }

    var imageWidth: CGFloat = 50
    /**
     Calculate the rectangle for the placeholder
     - parameter bounds: The current bounds of the placeholder
     - returns: The rectangle that the placeholder should render in
     */
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        //let titleHeight = self.titleHeight()
        //let lineHeight = self.selectedLineHeight
        //let rect = CGRect(x: 0, y: titleHeight, width: bounds.size.width, height: bounds.size.height - titleHeight - lineHeight)

        // Place holder text should be in center of textfield
        let y = boxHeight / 4
        var width = bounds.size.width
        var x: CGFloat = 10
        if startingImage != nil {
            x += imageWidth
            width -= imageWidth
        }
        let rect = CGRect(x: x, y: y, width: width - 20, height: boxHeight)
        return rect
    }

    // MARK: - Positioning Overrides

    /**
    Calculate the bounds for the title label. Override to create a custom size title field.
    - parameter bounds: The current bounds of the title
    - parameter editing: True if the control is selected or highlighted
    - returns: The rectangle that the title label should render in
    */
    open func titleLabelRectForBounds(_ bounds:CGRect, editing:Bool) -> CGRect {
        let titleHeight = self.titleHeight()
        if editing {
            return CGRect(x: 8, y: 12, width: bounds.size.width, height: titleHeight)
        }
        return CGRect(x: 8, y: titleHeight, width: bounds.size.width, height: titleHeight)
    }

    /**
     Calculate the bounds for the bottom line of the control. Override to create a custom size bottom line in the textbox.
     - parameter bounds: The current bounds of the line
     - parameter editing: True if the control is selected or highlighted
     - returns: The rectangle that the line bar should render in
     */
    open func lineViewRectForBounds(_ bounds:CGRect, editing:Bool) -> CGRect {
        //let lineHeight:CGFloat = editing ? CGFloat(self.selectedLineHeight) : CGFloat(self.lineHeight)
        //return CGRect(x: 0, y: bounds.size.height - lineHeight, width: bounds.size.width, height: lineHeight);
        return CGRect(x: 0, y: 0, width: bounds.size.width, height: 65);
    }

    /**
     Calculate the height of the title label.
     -returns: the calculated height of the title label. Override to size the title with a different height
     */
    open func titleHeight() -> CGFloat {
        if let titleLabel = self.titleLabel,
            let font = titleLabel.font {
                return font.lineHeight
        }
        return 15.0
    }

    /**
     Calcualte the height of the textfield.
     -returns: the calculated height of the textfield. Override to size the textfield with a different height
     */
    open func textHeight() -> CGFloat {
        return self.font!.lineHeight + 7.0
    }

    // MARK: - Layout

    /// Invoked when the interface builder renders the control
    override open func prepareForInterfaceBuilder() {
        if #available(iOS 8.0, *) {
            super.prepareForInterfaceBuilder()
        }
        
        self.borderStyle = .none
        
        self.isSelected = true
        _renderingInInterfaceBuilder = true
        self.updateControl(false)
        self.invalidateIntrinsicContentSize()
    }

    /// Invoked by layoutIfNeeded automatically
    override open func layoutSubviews() {
        super.layoutSubviews()

        self.titleLabel.frame = self.titleLabelRectForBounds(self.bounds, editing: self.isTitleVisible() || _renderingInInterfaceBuilder)
        //self.lineView.frame = self.lineViewRectForBounds(self.bounds, editing: self.editingOrSelected || _renderingInInterfaceBuilder)
        self.boxView.frame = self.lineViewRectForBounds(self.bounds, editing: self.editingOrSelected || _renderingInInterfaceBuilder)
    }

    /**
     Calculate the content size for auto layout

     - returns: the content size to be used for auto layout
     */
    override open var intrinsicContentSize : CGSize {
        return CGSize(width: self.bounds.size.width, height: self.titleHeight() + self.textHeight())
    }

    // MARK: - Helpers

    fileprivate func titleOrPlaceholder() -> String? {
        if let title = self.title ?? self.placeholder {
            return self.titleFormatter(title)
        }
        return nil
    }
/*
    fileprivate func selectedTitleOrTitlePlaceholder() -> String? {
        if let title = self.selectedTitle ?? self.title ?? self.placeholder {
            return self.titleFormatter(title)
        }
        return nil
    }*/
}
