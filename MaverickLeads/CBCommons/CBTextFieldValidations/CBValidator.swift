//
//  CBValidator.swift
//  CB Utils
//
//  Created by Developer on 12/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import UIKit

class CBValidator: NSObject {
    
    deinit {
        print("Deinit called: \(String(describing: self))")
    }
    
    private var validationObjects: [ValidationObject]
    private var selectedIndex: Int = 0
    private var view: UIView
    
    private var scrollView: UIScrollView? {
        didSet {
            scrollView?.delegate = self
        }
    }
    
    private var validationDelegate: CBTextValidationDelegate
    
    init(withView: UIView, delegate: CBTextValidationDelegate) {
        view = withView
        validationObjects = []
        validationDelegate = delegate
    }
    
    func addScrollView(_ scrollView: UIScrollView) {
        self.scrollView = scrollView
    }
    
    func add(_ vObjects: ValidationObject ...) {
        validationObjects = vObjects
        var lastTextField: SkyFloatingLabelTextField?
        for validationObject in validationObjects {
            lastTextField?.returnKeyType = .next
            addSettings(textField: validationObject.textField)
            lastTextField = validationObject.textField
        }
        
    }
    
    func addSettings(textField: SkyFloatingLabelTextField) {
        textField.delegate = self
        textField.returnKeyType = .done
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.addTarget(self, action: #selector(editingBegin(_:)), for: .editingDidBegin)
    }
    
    func removeRules(textField: CBTextField) {
        for validationObject in validationObjects {
            if textField == validationObject.textField {
                validationObject.rules = []
            }
        }
    }
    
    func addRules(textField: CBTextField, rules: [CBValidationType]) {
        for validationObject in validationObjects {
            if textField == validationObject.textField {
                validationObject.rules = rules
            }
        }
    }
    
//    func onValidationChange(successEvent: @escaping SuccessEvent) {
//        scrollView?.delegate = self
//
//        validationChanged = successEvent
//    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        //nextButton.isEnabled = areFieldsValid()
        if validationObjects[selectedIndex].rules.contains(where: { $0.isPhoneNumber() }) {
            textField.text = textField.text?.formatPhoneNumber()
        }
        validate()
    }
    
    func validate() {
        let valid = checkValidation()
        //        mainButton?.isEnabled = valid
        //        accessoryButton?.isEnabled = valid
        //        accessoryButton?.layer.cornerRadius = 0
        validationDelegate.onValidationChanged(areFieldsValid: valid)
        
    }
    
    private func checkValidation() -> Bool {
        var isValid = true
        for validationObject in validationObjects {
            let valid = validationObject.isValid()
            print("Validation Object: \(validationObject.textField.placeholder ?? ""), isValid: \(valid)")
            isValid = isValid && valid
        }
        return isValid
    }
    
    @objc func editingBegin(_ textField: SkyFloatingLabelTextField) {
        if let index = validationObjects.getIndex(predicate: { $0.textField == textField }) {
            selectedIndex = index
        }
        validationDelegate.didBeginEditing(textField)
        scrollView?.scrollToView(view: textField, animated: true)
        
        validate()
    }
    
    func getSelectedTextField() -> SkyFloatingLabelTextField {
        return validationObjects[selectedIndex].textField
    }
    
    
    
    func expirationCheck(oldText: String, newText: String, newLength: Int) -> Bool {
        let oldLength: Int = oldText.count
        if newText == "" {
        } else if oldLength == 0 {
            
            return "01".contains(newText)
            
        } else if oldLength == 1 {
            if oldText == "0" {
                return "123456789".contains(newText)
            } else {
                return "012".contains(newText)
            }
        }
        return newLength < 6
    }
    
}

extension CBValidator: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if selectedIndex == validationObjects.count - 1 {
            view.endEditing(true)
        } else {
            if validationObjects[selectedIndex].shouldReturn {
                validationObjects[selectedIndex + 1].textField.becomeFirstResponder()
                
            } else {
                if validationObjects[selectedIndex+1].textField.text == "" {
                    validationObjects[selectedIndex+1].textField.text = " "
                }
                validationDelegate.onReturn(textField: validationObjects[selectedIndex].textField)
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength: Int = (textField.text?.count ?? 0) + string.count - range.length
        let selectedObject = validationObjects[selectedIndex]
        let oldText = selectedObject.textField.text ?? ""
        
        if (oldText == nil || (textField.text?.isEmpty ?? true)) && string.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            return false
        } else {
            return selectedObject.rules.shouldChangeCharacter(text: string, oldText: oldText, newLength: newLength)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let selectedTextField = validationObjects[selectedIndex].textField
        validationDelegate.onFocusLost(
            textField: selectedTextField,
            hasError: selectedTextField.hasErrorMessage
        )
        
        validate()
    }
    
}

extension CBValidator: UIScrollViewDelegate {
    
    // This method is set to hide keyboard on scrolling
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
}
