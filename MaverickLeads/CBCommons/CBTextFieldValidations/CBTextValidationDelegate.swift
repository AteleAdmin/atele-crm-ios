//
//  CBTextValidationDelegate.swift
//  CB Utils
//
//  Created by Developer on 12/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

protocol CBTextValidationDelegate {
    func onFocusLost(textField: SkyFloatingLabelTextField, hasError: Bool)
    func onReturn(textField: SkyFloatingLabelTextField)
    func onValidationChanged(areFieldsValid: Bool)
    func didBeginEditing(_ textField: SkyFloatingLabelTextField)
}

extension CBTextValidationDelegate {
    
    func onFocusLost(textField: SkyFloatingLabelTextField, hasError: Bool) {
        
    }
    
    func onReturn(textField: SkyFloatingLabelTextField) {
        
    }
    
    func didBeginEditing(_ textField: SkyFloatingLabelTextField) {
        
    }
}
