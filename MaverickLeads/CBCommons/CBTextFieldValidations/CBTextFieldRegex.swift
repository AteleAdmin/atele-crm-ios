//
//  CBTextFieldRegex.swift
//  CB Utils
//
//  Created by Developer on 12/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import UIKit

enum CBTextFieldRegex {
    case email, name, userName, password, phone, usStates, securityCode, alphaNumeric, alphaNumericWithSpace, gatecode, emailAndUsername
    case custom(regex: String)
    
    case number, space, alphabets, accent, special
    case compound([CBTextFieldRegex])
    
    func getKeyboardType() -> UIKeyboardType {
        switch self {
        case .phone, .gatecode, .securityCode:
            return .numberPad
        case .email, .emailAndUsername, .userName:
            return .emailAddress
        default:
            return .default
        }
    }
    
    private func get() -> String {
        switch self {
        case.userName:
            return "^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        case .emailAndUsername:
            return "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}|^[a-zA-Z][a-zA-Z\\s]*$"
        case .name:
            let accentChars = "èéêëēėęÈÉÊËĒĖĘÿŸûüùúūÛÜÙÚŪîïíīįìÎÏÍĪĮÌôöòóœøōõÔÖÒÓŒØŌÕàáâäæãåāÀÁÂÄÆÃÅĀßśšŚŠłŁžźżŽŹŻçćčÇĆČñńÑŃ"
            return "^[-'a-zA-Z\(accentChars) ]+$"
        case .email:
            return "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        case .password:
            return "^(?=.*[A-Za-z])(?=.*[^A-Za-z]).{8,20}$"
        //return "^(?=.*[A-Za-z])(?=.*[0-9]).{8,20}$"
        case .phone:
            return "^((\\+)|(00))[0-9]{6,14}$"
        case .usStates:
            return "^(?:(A[AEKLPRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]))$"
        case .securityCode:
            return "^[0-9]{3,4}$"
        case .alphaNumeric:
            return "^[a-zA-Z0-9]*$"
        case .alphaNumericWithSpace:
            return "^[a-zA-Z0-9 ]*$"
        case .gatecode:
            return "^[0-9*#]*$"
        case .number:
            return "^[0-9]*$"
        case .space:
            return "^[ ]*$"
        case .alphabets:
            return "^[a-zA-Z]*$"
        case .accent:
            let accentChars = "èéêëēėęÈÉÊËĒĖĘÿŸûüùúūÛÜÙÚŪîïíīįìÎÏÍĪĮÌôöòóœøōõÔÖÒÓŒØŌÕàáâäæãåāÀÁÂÄÆÃÅĀßśšŚŠłŁžźżŽŹŻçćčÇĆČñńÑŃ"
            return "^[\(accentChars)]*$"
        case .special:
            return "^[!_\"&‘’'-.]*$"
        case .custom(regex: let reg):
            return reg
        case .compound(let regexes):
            var finalResult = "^["
            for regex in regexes {
                let string = regex.get()
                    .replacingOccurrences(of: "^[", with: "")
                    .replacingOccurrences(of: "]*$", with: "")
                finalResult += string
            }
            finalResult += "]*$"
            return finalResult
        }
    }
    
    
    func check(string: String) -> Bool {
        let tester = NSPredicate(format:"SELF MATCHES %@", get())
        return tester.evaluate(with: string)
    }
}
