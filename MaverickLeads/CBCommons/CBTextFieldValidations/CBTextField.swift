//
//  CBTextField.swift
//  CB Utils
//
//  Created by Developer on 12/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

open class CBTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    open var hasErrorMessage: Bool = false {
        didSet {
            changeUI()
        }
    }

    func changeUI() {
        // For Demo
        if hasErrorMessage {
            backgroundColor = .red
        } else {
            backgroundColor = .white
        }
    }
    
}
