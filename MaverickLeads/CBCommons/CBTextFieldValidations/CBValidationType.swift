//
//  CBValidationType.swift
//  CB Utils
//
//  Created by Developer on 12/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

enum CBValidationType {
    case regex(CBTextFieldRegex)
    case anyRegexFrom([CBTextFieldRegex])
    case maxLength(Int)
    case minLength(Int)
    case matches(UITextField)
    case expiration
    case creditCard
    case notEmpty
    case phoneNumber
    case alphaNumeric
    case gateCode
    case limitCharacters(String)
    
    func getKeyboardType() -> UIKeyboardType {
        switch self {
        case .gateCode, .phoneNumber, .creditCard, .expiration:
            return .numberPad
        case .regex(let regex):
            return regex.getKeyboardType()
        default:
            return .default
        }
    }
    
    func checkRule(on textField: UITextField) -> Bool {
        let text = textField.text ?? ""
        switch self {
        case .regex(let reg):
            return reg.check(string: text)
        case .anyRegexFrom(let regexes):
            var result = false
            for regex in regexes {
                result = result || regex.check(string: text)
            }
            return result
        
        case .limitCharacters(let customCharacterString):
            return !customCharacterString.contains(where: {text.contains($0)})
            
        case .maxLength(let max):
            return max >= text.count
        case .minLength(let min):
            return min <= text.count
        case .matches(let matchText):
            return textField.text == matchText.text
        case .creditCard:
            return text.count > 15
        case .expiration:
            return text.count > 4
        case .notEmpty:
            return textField.text != ""
        case .phoneNumber:
            return text.count > 13
        case .alphaNumeric:
            return true
        case .gateCode:
            return true
        }
    }
    
    func getMaxLength() -> Int? {
        switch self {
        case .maxLength(let max):
            return max
        default:
            return nil
        }
    }
    
    func isExpiration() -> Bool {
        switch self {
        case .expiration:
            return true
        default:
            return false;
        }
    }
    func isAlphaNumeric() -> Bool {
        switch self {
        case .alphaNumeric:
            return true
        default:
            return false;
        }
    }
    
    func isCreditCard() -> Bool {
        switch self {
        case .creditCard:
            return true
        default:
            return false;
        }
    }
    func isPhoneNumber() -> Bool {
        switch self {
        case .phoneNumber:
            return true
        default:
            return false;
        }
    }
    
    func isGateCode() -> Bool {
        switch self {
        case .gateCode:
            return true
        default:
            return false;
        }
    }
    
    func isName() -> Bool {
        switch self {
        case .regex(.name):
            return true
        default:
            return false
        }
    }
    
}

extension Array where Element == CBValidationType {
    func shouldChangeCharacter(text: String, oldText: String, newLength: Int) -> Bool {
        var result = true
        
        for rule in self {
            switch rule {
            case .alphaNumeric:
                result = result && CBTextFieldRegex.alphaNumeric.check(string: text)
            case .creditCard:
                result = result && newLength < 20
            case .gateCode:
                result = result && CBTextFieldRegex.gatecode.check(string: text)
            case .maxLength(let maxLength):
                result = result && maxLength >= newLength
            case .expiration:
                result = result && expirationCheck(oldText: oldText, newText: text, newLength: newLength)
            case .phoneNumber:
                result = result && newLength < 15
            default:
                print("\(rule) does not have length constraint")
            }
        }
        return result
    }
    
    private func expirationCheck(oldText: String, newText: String, newLength: Int) -> Bool {
        let oldLength: Int = oldText.count
        if newText == "" {
        } else if oldLength == 0 {
            
            return "01".contains(newText)
            
        } else if oldLength == 1 {
            if oldText == "0" {
                return "123456789".contains(newText)
            } else {
                return "012".contains(newText)
            }
        }
        return newLength < 6
    }
}

extension Array where Element: Equatable {
    
    func getIndex(of element: Element) -> Int? {
        for i in 0 ..< count {
            if self[i] == element {
                return i
            }
        }
        return nil
    }
}

extension Array {
    
    func getIndex(predicate: @escaping (Element) -> Bool) -> Int? {
        for i in 0 ..< count {
            if predicate(self[i]) {
                return i
            }
        }
        return nil
    }
}
