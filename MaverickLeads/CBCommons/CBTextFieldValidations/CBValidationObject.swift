//
//  CBValidationObject.swift
//  CB Utils
//
//  Created by Developer on 12/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

class ValidationObject {
    
    var textField: SkyFloatingLabelTextField
    var rules: [CBValidationType]
    var shouldReturn: Bool
    
    init(textField txt: SkyFloatingLabelTextField, implementDefaultReturn: Bool = true, rules rul: CBValidationType ...) {
        textField = txt
        rules = rul
        shouldReturn = implementDefaultReturn
        textField.keyboardType = rules.first?.getKeyboardType() ?? .default
    }
    
    func isValid() -> Bool {
        if rules.count == 0 {
            textField.hasErrorMessage = false
            print("rules count zero")
            return true
        }
        if (textField.text ?? "").count != 0 {
            for rule in rules {
                let valid = rule.checkRule(on: textField)
                print("rule: \(rule), text: \(textField.text ?? ""), isValid: \(valid)")
                if !valid {
                    textField.hasErrorMessage = !textField.isEditing && !valid
                    return false
                }
            }
        } else {
            textField.hasErrorMessage = false
            return false
        }
        
        if textField.text?.containsProfanity() == true {
            textField.hasErrorMessage = !textField.isEditing
            return false
        }
        
        textField.hasErrorMessage = false
        return true
    }
    
}
