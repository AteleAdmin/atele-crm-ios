//
//  PromisedFuture+CR.swift
//  MaverickLeads
//
//  Created by Developer on 20/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import PromisedFuture

extension Future {
    func crExecute(errorHandler: ((Error, MessageCodeFilter?) -> Void)? = nil, filter: MessageCodeFilter? = nil, onFailure failureHandler: FailureCompletion? = nil, onSuccess successHandler: @escaping SuccessCompletion) {
        execute(onSuccess: successHandler) { (error) in
            errorHandler?(error, filter)
            failureHandler?(error)
        }
    }
    
    func crExecuteWith(errorHandler: ((Error, MessageCodeFilter?) -> Void)? = nil, filters: [CodeFilter] = [], onFailure failureHandler: FailureCompletion? = nil, onSuccess successHandler: @escaping SuccessCompletion) {
        let filter: MessageCodeFilter = { messageCode in
            for filter in filters {
                if messageCode.code == filter.code {
                    filter.block()
                    return true
                }
            }
            return false
        }
        execute(onSuccess: successHandler) { (error) in
            errorHandler?(error, filter)
            failureHandler?(error)
        }
    }
    
}

class CodeFilter {
    var block: () -> Void
    var code: Int
    init(statusCode: Int, completion: @escaping () -> Void) {
        code = statusCode
        block = completion
    }
}
