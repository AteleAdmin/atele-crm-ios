//
//  ErrorHandler.swift
//  MaverickLeads
//
//  Created by Developer on 20/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import UIKit

typealias MessageCodeTuple = (message: String, code: Int)
typealias MessageCodeFilter = (MessageCodeTuple) -> Bool

protocol ErrorHandler {
    var errorHandler: (Error, MessageCodeFilter?) -> Void { get }
}

enum CBErrorType: Error {
    case noInternet, statusCode(code: Int), crServer(MessageCodeTuple), unKnown(message: String)
}

extension ErrorHandler {
    
    var errorHandler: (Error, MessageCodeFilter?) -> Void {
        return { error, filter in
            if let crError = error as? CBErrorType {
                switch crError {
                case .noInternet:
                    debugPrint("")
                    // Show Alert
                    
                case .crServer(let messageCode):
                    if (messageCode.code == 401) {
                        debugPrint("")
                        // Show Alert
                        
                    } else if filter?(messageCode) != true {
                        debugPrint("")
                        // Show Alert
                    }
                    
                case .unKnown(let message):
                    debugPrint("")
                    // Show Alert
                    
                case .statusCode(let code):
                    switch code {
                    case 401:
                        debugPrint("")
                        // Show Alert
                        
                    case 400:
                        debugPrint("")
                        // Show Alert
                        
                    case -1001, 408:
                        debugPrint("")
                        // Show Alert
                        
                    default:
                        debugPrint("")
                        // Show Alert
                    }
                    
                }
            }
        }
    }
}
