//
//  CBAuthHandler.swift
//  MaverickLeads
//
//  Created by Developer on 18/07/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import Alamofire

class CBAuthHandler {
    
    
    // MARK: - Class Properties
    
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration)
    }()
    private let lock = NSLock()
    private var requestsToRetry: [RequestRetryCompletion] = []
    private var isRefreshing = false
    private var baseURLString: String
    private var accessToken: String {
        return ""
    }
    private var refreshToken: String {
        return ""
    }
    
    
    // MARK: - Initialization
    
    public init(baseURLString: String) {
        self.baseURLString = baseURLString
    }
}


// MARK: - Request Adpter

extension CBAuthHandler: RequestRetrier {
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock()
        defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse,
            response.statusCode == 401 {
            
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let self = self else { return }
                    self.lock.lock()
                    defer { self.lock.unlock() }
                    self.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    self.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
}


// MARK: - Request Adpter

extension CBAuthHandler: RequestAdapter {
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let urlString = urlRequest.url?.absoluteString,
            urlString.hasPrefix(baseURLString) {
            var urlRequest = urlRequest
            debugPrint("----Setting Auth Tocken----")
            urlRequest.setValue(accessToken, forHTTPHeaderField: "Authorization")
            debugPrint("Auth Token: \(accessToken)")
            return urlRequest
        }
        return urlRequest
    }
}


// MARK: - Private Methods

extension CBAuthHandler {
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
        let urlString = "\(baseURLString)api/users/refreshtoken"
        let parameters: [String: Any] = ["refreshToken": refreshToken]
        let headers: HTTPHeaders = ["Authorization": accessToken]
        
        sessionManager.request(urlString, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseDecodableObject { [weak self] (response: DataResponse<ServiceResponse<Empty>>) in
            
            guard let self = self else { return }
            
            debugPrint("PASSED PARAMETERS: \(String(describing: parameters))")
            debugPrint("PASSED HEADERS: \(String(describing: headers))")
            debugPrint("********************************************************\n\n")
            
            switch response.result {
            case .success(let value):
                if value.success == true {
                    let user = value.data!
                    completion(true, "token here", "refreshToken Here")
                } else {
                    completion(false, nil, nil)
                }
            case .failure(let error):
                debugPrint("...:::Refresh Token Error:::...")
                debugPrint("Error: \(String(describing: error))")
                completion(false, nil, nil)
            }
            
            self.isRefreshing = false
        }
    }
}
