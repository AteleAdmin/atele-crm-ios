//
//  CBServiceRouter.swift
//  MaverickLeads
//
//  Created by Developer on 19/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Alamofire

protocol CBServiceRouter {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
    var isAuthToken: Bool { get }
    var isPagingRequest: Bool { get }
}

extension CBServiceRouter {
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var isAuthToken: Bool {
        return false
    }
    
    var isPagingRequest: Bool {
        return false
    }
    
}
