//
//  MLServiceManager.swift
//  MaverickLeads
//
//  Created by Developer on 10/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation
import Alamofire
import Reachability
import PromisedFuture
import MBProgressHUD

class MLServiceManager {
    
    @discardableResult
    static func performRequest<T: Codable>(router: CBServiceRouter, showProgress: Bool = true, on viewController: UIViewController = UIViewController.topViewController, refreshControl: UIRefreshControl? = nil, decoder: JSONDecoder = JSONDecoder()) -> Future<T?> {
        let future: Future<T> = performRequestWithCompleteResponse(router: router, showProgress: showProgress, on: viewController, refreshControl: refreshControl, decoder: decoder)
        
        
        return Future(operation: { (completion) in
            future.execute(onSuccess: { (response) in
                completion(.success(response))
            }, onFailure: { (error) in
                completion(.failure(error))
            })
        })
    }
    
    
    @discardableResult
    static func performRequestWithCompleteResponse<T: Codable>(router: CBServiceRouter, showProgress: Bool = true, on viewController: UIViewController = UIViewController.topViewController, refreshControl: UIRefreshControl? = nil, decoder: JSONDecoder = JSONDecoder()) -> Future<T> {
        var hud: MBProgressHUD?
        
        if showProgress {
            if refreshControl != nil {
                refreshControl?.beginRefreshing()
            } else {
                let topController = viewController
                hud = MBProgressHUD.showLoadingHudAddedTo(view: topController.view)
                if let hud = hud {
                    hud.label.text = "Loading..."
                    hud.show(animated: true)
                }
            }
        }
        return Future(operation: { completion in
            if isConnected() {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                addOauthHandler()
                var baseUrl = "\(APILinks.server)api/"
                
                let urlString = "\(baseUrl)\(router.path)"
                sessionManager.request(urlString, method: router.method, parameters: router.parameters, encoding: URLEncoding.queryString, headers: router.headers).responseDecodableObject { (response: DataResponse<T>) in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    debugPrint("PASSED PARAMETERS: \(String(describing: router.parameters))")
                    debugPrint("PASSED HEADERS: \(String(describing: router.headers))")
                    debugPrint("********************************************************\n\n")
                    if showProgress {
                        if refreshControl != nil {
                            DispatchQueue.main.async {
                                refreshControl?.endRefreshing()
                            }
                        } else {
                            if let hud = hud {
                                hud.hide(animated: true)
                            }
                        }
                    }
                    switch response.result {
                    case .success(let value):
                        completion(.success(value))
                        
                    case .failure(let error):
                        if let statusCode = response.response?.statusCode, statusCode != 200 {
                            completion(.failure(CBErrorType.statusCode(code: statusCode)))
                        } else if (error as NSError).code == -1009 {
                            completion(.failure(CBErrorType.noInternet))
                        } else {
                            completion(.failure(CBErrorType.unKnown(message: error.localizedDescription)))
                        }
                    }
                    }.validate()
            } else {
                if showProgress {
                    if refreshControl != nil {
                        refreshControl?.endRefreshing()
                    } else {
                        if let hud = hud {
                            hud.hide(animated: true)
                        }
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    completion(.failure(CBErrorType.noInternet))
                })
            }
        })
    }
    
    private static func isConnected() -> Bool {
        let reachibility = Reachability()
        let networkStatus = reachibility?.connection
        
        return networkStatus != Reachability.Connection.none
    }
    
    private static func addOauthHandler() {
        // If User Exists or saved in app
        /*
         if let _ = AppUserDefaults.user.get() {
         let authhandler = CBAuthHandler(baseURLString: APILinks.server)
         if sessionManager.retrier == nil {
         sessionManager.retrier = authhandler
         }
         if sessionManager.adapter == nil {
         sessionManager.adapter = authhandler
         }
         }
         */
    }
    
    private static let sessionManager: SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            APILinks.server: .disableEvaluation
        ]
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        let manager = Alamofire.SessionManager(configuration: configuration,
                                               serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
        manager.session.configuration.timeoutIntervalForRequest = 30
        manager.session.configuration.timeoutIntervalForResource = 30
        manager.delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .cancelAuthenticationChallenge
                } else {
                    credential = manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            
            return (disposition, credential)
        }
        return manager
    }()
}
