//
//  LeadRoutes.swift
//  MaverickLeads
//
//  Created by Developer on 12/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

import Alamofire

class MLTeamLeadQuery {
    let leadId: Int?
    let producerId: Int?
    let agencyOwnerId: Int?
    let fromDate: String?
    let toDate: String?
    
    // Date formats will be string (Default Empty or Null,No Space e.g 02/22/2020, mm/dd/yyyy)
    
    init(from fDate: String?, to tDate: String?, leadId: Int? = nil, producerId: Int? = nil, agencyOwnerId: Int? = nil) {
        self.leadId = leadId
        self.producerId = producerId
        self.agencyOwnerId = agencyOwnerId
        fromDate = fDate
        toDate = tDate
    }
    
    func getAsParams() -> [String: Any] {
        if let lId = leadId {
            return [
                "teamLeadId": lId,
                "from": fromDate ?? "",
                "to": toDate ?? ""
            ]
        } else {
            
            var params: [String: Any] = [
                "producerId": 0,
                "agencyOwnerId": agencyOwnerId ?? 0,
                "from": fromDate ?? "",
                "to": toDate ?? ""
            ]
            
            let user = AppUserDefaults.user.get()!
            
            if user.uRole == UserRole.superAdmin {
                params["teamLeadId"] = 0
                params["agencyOwnerId"] = 0
            } else if user.uRole == .teamLead {
                params["teamLeadId"] = user.uID
            } else if user.uRole == .agencyOwner {
                params["agencyOwnerId"] = user.uID
            } else if user.uRole == .agencyProducer {
                params["producerId"] = user.uID
            }
            
            return params
        }
    }
}

enum MLDetailAPIs {
    
    case update(info: MLLeadDetails)  // 1
    case details(leadId: Int) // [MLLeadDetails]
    
    func  getParameters(isLive: Bool) -> Parameters? {
        switch self {
        case .update(let leadInfo):
            return leadInfo.getAsParams()
        case .details(let leadId):
            return ["leadId": leadId]
        }
    }
    
    var path: String {
        switch self {
        case .update:
            return "lead/post"
        case .details:
            return "lead-detail"
        }
    }
    
}

enum GeneralAPIs {
    case leads(query: MLTeamLeadQuery) // [MLLead] with few
    
    case totalCount(query: MLTeamLeadQuery) // [MLCountResponse] without name
    case acceptedCount(query: MLTeamLeadQuery) // [MLCountResponse] without name
    case rejectedCount(query: MLTeamLeadQuery) // [MLCountResponse] without name
    
    case totalSummary(query: MLTeamLeadQuery) // [MLCountResponse]
    case acceptedSummary(query: MLTeamLeadQuery) // [MLCountResponse]
    case rejectedSummary(query: MLTeamLeadQuery) // [MLCountResponse]
    
    case leadWise(query: MLTeamLeadQuery) // [MLCountResponse]
    case leadWiseAccepted(query: MLTeamLeadQuery) // [MLCountResponse]
    case leadWiseRejected(query: MLTeamLeadQuery) // [MLCountResponse]
    
    var parameters: Parameters? {
        switch self {
        case .leads(let query):
            return query.getAsParams()
        case .totalCount(let query):
            return query.getAsParams()
        case .acceptedCount(let query):
            return query.getAsParams()
        case .rejectedCount(let query):
            return query.getAsParams()
        
        case .totalSummary(let query):
            return query.getAsParams()
        case .acceptedSummary(let query):
            return query.getAsParams()
        case .rejectedSummary(let query):
            return query.getAsParams()
            
        case .leadWise(let query):
            return query.getAsParams()
        case .leadWiseAccepted(let query):
            return query.getAsParams()
        case .leadWiseRejected(let query):
            return query.getAsParams()
        }
    }
    
    var path: String {
        switch self {
            
        case .leads:
            return "leads"
            
        case .totalCount:
            return "count/total"
        case .acceptedCount:
            return "count/accepted"
        case .rejectedCount:
            return "count/rejected"
        
        case .totalSummary:
            return "summary/total"
        case .acceptedSummary:
            return "summary/accepted"
        case .rejectedSummary:
            return "summary/rejected"
            
        case .leadWise:
            return "total"
        case .leadWiseAccepted:
            return "accepted"
        case .leadWiseRejected:
            return "rejected"
        }
    }
}

enum LeadRouter: CBServiceRouter {
    case dipositionList // Array of MLDeposition
    case ownerList // [MLOwner] //(Bind Dropdown List e.g User Id, Name)
    case general(category: APICategory, api: GeneralAPIs)
    case liveAgencyCount(query: MLTeamLeadQuery) // [MLAgencyCount]
    case outsourceAgencyCount(outSourceOwnerId: Int, fromDate: String?, toDate: String?) // [MLAgencyCount]
    
    case live(details: MLDetailAPIs)
    case pingnpost(details: MLDetailAPIs)
    case pnpLive(details: MLDetailAPIs)
    case agencyProducer(agencyOwnerId: Int, producerId: Int)// [MLProducer]
    
    
    var method: HTTPMethod {
        return .post
    }
    
    private func getPath(for envType: String, apiDetails: MLDetailAPIs) -> String {
        switch apiDetails {
        case .update:
            return apiDetails.path + "/" + envType
        default:
            return envType + "/" + apiDetails.path
        }
    }
    
    var path: String {
        switch self {
        case .agencyProducer:
            return "agency/producer"
        case .outsourceAgencyCount:
            return "outsource/agency/count"
        case .liveAgencyCount:
            return "live/agency/count"
        case .ownerList:
            return "outsource/owner/list"
        case .dipositionList:
            return "disposition/list"
        case .general(let category, let api):
            return category.getPath() + api.path
        case .live(let detailAPIs):
            return getPath(for: "live", apiDetails: detailAPIs)
        case .pingnpost(let detailAPIs):
            return getPath(for: "pnp", apiDetails: detailAPIs)
        case .pnpLive(let detailAPIs):
            return getPath(for: "livepnp", apiDetails: detailAPIs)
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .general(_, let api):
            return api.parameters
        case .live(let detailAPIs):
            return detailAPIs.getParameters(isLive: true)
        case .pingnpost(let detailAPIs):
            return detailAPIs.getParameters(isLive: false)
        case .pnpLive(let detailAPIs):
            return detailAPIs.getParameters(isLive: false)
        case .liveAgencyCount(let query):
            return query.getAsParams()
        
        case .outsourceAgencyCount(let outSourceOwnerId, let fromDate, let toDate):
            return [
                "outsourceOwnerId": outSourceOwnerId,
                "from": fromDate,
                "to": toDate
            ]
            
        case .agencyProducer(let agencyOwnerId, let producerId):
            return ["agencyOwnerId":agencyOwnerId, "producerId": producerId]

        default:
            return nil
        }
    }
}

enum APICategory {
    case pnpTl, liveTl, pnpAgent, liveAgent, pnpAgency, liveAgency, outsource, pnp, live, livePnp, livePnpAgency, livePnpAgent, livePnpTl
    
    func getDetailAPI(fromLead lead: MLLead) -> LeadRouter {
        switch self {
        case .pnp,.pnpAgent, .pnpTl, .pnpAgency:
            return LeadRouter.pingnpost(details: MLDetailAPIs.details(leadId: lead.id))
        case .livePnp, .livePnpAgency, .livePnpAgent, .livePnpTl:
            return LeadRouter.pnpLive(details: MLDetailAPIs.details(leadId: lead.id))
        default:
            return LeadRouter.live(details: MLDetailAPIs.details(leadId: lead.id))
        }
    }
    
    func getLeadRouter(fromDetail detail: MLDetailAPIs) -> LeadRouter {
        switch self {
        case .pnp, .pnpAgent, .pnpTl, .pnpAgency, .livePnp, .livePnpTl, .livePnpAgency, .livePnpAgent:
            return .pingnpost(details: detail)
        default:
            return .live(details: detail)
        }
    }
    
    static func getCatFrom(superAccess: UserSuperAccess, subAccess: UserSubAccess) -> APICategory {
        
        switch superAccess {
            
        case .livePnp:
            switch subAccess {
            case .liveTransfers:
                return .livePnp
            case .agencyReport:
                return .livePnpAgency
            case .agentReport:
                return .livePnpAgent
            
            default:
                return .livePnp
            }
            
        case .liveLeads:
            
            switch subAccess {
            case .liveTransfers:
                return .live
            case .agencyReport:
                return .liveAgency
            case .agentReport:
                return .liveAgent
            case .tlNewLeads:
                return .liveTl
            default:
                return .live
            }
        
        case .pingAndPost:
            
            switch subAccess {
            case .pingAndPost:
                return .pnp
            case .agencyReport:
                return .pnpAgency
            case .agentReport:
                return .pnpAgent

            case .tlNewLeads:
                return .pnpTl
            
            case .pnpAgencyReport:
                return .pnpAgency
            
            default:
                return .pnp
            }
        
        case .newLead:
            return .liveTl
            
        case .liveTransfers:
            return .live
            
        case .outsourcePanel:
            return .outsource
            
        case .leads:
            switch subAccess {
            case .tlNewLeads:
                return .liveTl
            case .pingAndPost:
                return .pnp
            case .agencyReport:
                return .liveAgency
            
            default:
                return .live
            }
            
        default:
            return .outsource
        }
        
    }
    
    func getPath() -> String {
        switch self {
        case .live:
            return "live/"
        case .pnp:
            return "pingnpost/"
        case .liveAgency:
            return "live/agency/"
        case .pnpAgency:
            return "pingnpost/agency/"
        case .liveAgent:
            return "live/agent/"
        case .pnpAgent:
            return "pingnpost/agent/"
        case .liveTl:
            return "live/tl/"
        case .pnpTl:
            return "pingnpost/tl/"
        case .outsource:
            return "outsource/"
        case .livePnp:
            return "livepnp/"
        case .livePnpAgency:
            return "livepnp/agency/"
        case .livePnpAgent:
            return "livepnp/agent/"
        case .livePnpTl:
            return "livepnp/tl/"
        }
    }
}


/*

leads?
api/pingnpost/tl/leads?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/tl/leads?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/agent/leads?teamLeadId={teamLeadId}&from={from}&to={to}
api/outsource/leads?outsourceOwnerId={outsourceOwnerId}&from={from}&to={to}
api/pingnpost/agency/leads?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agency/leads?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agent/leads?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/leads?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
api/live/leads?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}


count/total?
api/pingnpost/tl/count/total?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/tl/count/total?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/agent/count/total?teamLeadId={teamLeadId}&from={from}&to={to}
api/outsource/count/total?outsourceOwnerId={outsourceOwnerId}&from={from}&to={to}
api/pingnpost/agency/count/total?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agency/count/total?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agent/count/total?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/leads/count/total?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
api/live/count/total?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}


 
count/accepted?

api/pingnpost/tl/count/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/tl/count/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/agent/count/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
api/outsource/count/accepted?outsourceOwnerId={outsourceOwnerId}&from={from}&to={to}
api/pingnpost/agency/count/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agency/count/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agent/count/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/leads/count/accepted?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
api/live/count/accepted?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
 
count/rejected?
api/pingnpost/tl/count/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/tl/count/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/agent/count/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
api/outsource/count/rejected?outsourceOwnerId={outsourceOwnerId}&from={from}&to={to}
api/pingnpost/agency/count/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agency/count/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
api/live/agent/count/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
api/pingnpost/leads/count/rejected?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
api/live/count/rejected?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
 
summary/accepted?
 api/pingnpost/tl/summary/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/tl/summary/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/agent/summary/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/outsource/summary/accepted?outsourceOwnerId={outsourceOwnerId}&from={from}&to={to}
 api/pingnpost/agency/summary/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/agency/summary/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/agent/summary/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/summary/accepted?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 api/live/summary/accepted?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
summary/rejected?
 api/pingnpost/tl/summary/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/tl/summary/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/agent/summary/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/outsource/summary/rejected?outsourceOwnerId={outsourceOwnerId}&from={from}&to={to}
 api/pingnpost/agency/summary/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/agency/summary/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/agent/summary/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/summary/rejected?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 api/live/summary/rejected?producerId={producerId}&agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
total?
 api/pingnpost/tl/total?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/tl/total?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/agent/summary/total?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/agency/total?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/agent/summary/total?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/producer/total?agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 api/live/producer/total?agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
 
 
accepted?
 api/pingnpost/tl/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/tl/accepted?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/producer/accepted?agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 api/live/producer/accepted?agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
rejected?
 api/pingnpost/tl/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/live/tl/rejected?teamLeadId={teamLeadId}&from={from}&to={to}
 api/pingnpost/producer/rejected?agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 api/live/producer/rejected?agencyOwnerId={agencyOwnerId}&from={from}&to={to}
 
 
*/
