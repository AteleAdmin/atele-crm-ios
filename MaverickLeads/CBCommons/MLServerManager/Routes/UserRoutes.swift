//
//  UserRoutes.swift
//  MaverickLeads
//
//  Created by Developer on 10/04/2020.
//  Copyright © 2020 MaverickLeads. All rights reserved.
//

import Foundation

import Alamofire

enum UserRouter: CBServiceRouter {
    case login(username: String, password: String)
    case getByRole(roleId: Int)
    case getByUser(userId: Int)
    case updatePushToken(userId: Int, userRole: String, token: String)
    
    var method: HTTPMethod {
        return .post
    }
    
    var path: String {
        switch self {
        case .login:
            return "account/login"
        case .getByRole:
            return "account/users-by-role"
        case .getByUser:
            return "account/users-by-reported"
        case .updatePushToken:
            return "account/notification"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .login(let username, let password):
            return ["username": username, "password": password]
        case .getByRole(let roleId):
            return ["roleId": roleId]
        case .getByUser(let userId):
            return ["userId": userId]
        case .updatePushToken(let userId, let userRole, let token):
            return ["uId": userId, "token": token, "role": userRole]
        }
    }
    
}
