//
//  CBNetworkManager.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import UIKit

import Reachability
import Alamofire
import MBProgressHUD

public enum ApiVersion: String {
    case V1 = "v1/"
}
public enum Method {
    case get, post, put, delete
    func getAlamofireMethod() -> Alamofire.HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        case .delete:
            return .delete
        }
    }
}

struct ServiceResponse<Value: Codable>: Codable {
    let success: Bool?
    let message: String
    let statusCode: Int?
    let data: Value?
    let metadata: PagingMetaData?
    
    enum CodingKeys: String, CodingKey {
        case success
        case message
        case statusCode
        case data
        case metadata
    }
}

struct PagingMetaData: Codable {
    let page: Int
    let limit: Int
    let count: Int
    let totalPages: Int
    let totalCount: Int
}

typealias ServiceEvent<Value: Codable> = (ServiceResponse<Value>) -> Void

class NetworkManager {
    
    class func makeCall<Value: Codable>(url: String, refreshControl: UIRefreshControl? = nil, refreshButton: UIButton? = nil, blocking: Bool = true, params: NSDictionary? = nil, type: Method = .get, completionHandler: ServiceEvent<Value>?) {
        
        makeCallGeneric(url: "\(APILinks.server)api/\(url)", refreshControl: refreshControl, refreshButton: refreshButton, blocking: blocking, params: params, type: type, completionHandler: completionHandler)
    }
    
    class func makeCallGeneric<Value: Codable>(url: String, refreshControl: UIRefreshControl? = nil, refreshButton: UIButton? = nil, blocking: Bool = true, params: NSDictionary? = nil, type: Method = .get, completionHandler: ((Value) -> Void)?) {
        
        if isConnected() {
            var hud = MBProgressHUD()
            
            refreshButton?.isHidden = true
            if (blocking ==  true) {
                UIApplication.shared.beginIgnoringInteractionEvents()
                if let refreshCon = refreshControl {
                    refreshCon.beginRefreshing()
                } else {
                    let topController = UIViewController.topViewController
                    hud = MBProgressHUD.showAdded(to: topController.view, animated: true)
                    hud.mode = MBProgressHUDMode.indeterminate
                    hud.label.text = "Loading..."
                    hud.show(animated: true)
                }
                
            }
            let fullUrlPath = "\(url)"

            let headers: [String: String] = [:]
            
            if let param = params as? Parameters? {
                let decoder = JSONDecoder()
                
                sessionManager.request(fullUrlPath, method: type.getAlamofireMethod(), parameters: param, encoding: (type == .get) ? URLEncoding.default : JSONEncoding.default, headers: headers).responseDecodableObject(keyPath: nil, decoder: decoder) { (response: DataResponse<Value>) in
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    print("Calling Endpoint: \(response.request?.url?.absoluteString ?? "EmptyURL")")
                    print("Passed Parameters: \(String(describing: param))")
                    print("Passed Header \(String(describing: headers))")
                    print("raw response: \(response)")
                    
                    if blocking == true {
                        if let refreshCon = refreshControl {
                            refreshCon.endRefreshing()
                        } else {
                            hud.hide(animated: true)
                        }
                    }
                    
                    switch response.result {
                    case .success(let value):
                        completionHandler?(value)
                    case .failure(let error):
                        refreshButton?.isHidden = false
                        
                        // Show Alert
                    }
                    
                }
            } else {
                // Show Alert
            }
        } else {
            refreshButton?.isHidden = false
            // Show Alert
            if let refreshCon = refreshControl {
                refreshCon.endRefreshing()
            }
        }
    }
    
    class public func isConnected() -> Bool {
        let reachibility = Reachability()
        let networkStatus = reachibility?.connection
        
        return networkStatus != Reachability.Connection.none
    }
    
    static let sessionManager: SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            APILinks.server: .disableEvaluation
        ]
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        let manager = Alamofire.SessionManager(configuration: configuration,
                                               serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
        manager.session.configuration.timeoutIntervalForRequest = 30
        manager.session.configuration.timeoutIntervalForResource = 30
        manager.delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .cancelAuthenticationChallenge
                } else {
                    credential = manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            
            return (disposition, credential)
        }
        return manager
    }()
}
