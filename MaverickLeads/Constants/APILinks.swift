//
//  APILinks.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

struct APILinks {
    #if DEVELOPEMENT
    static let server = "http://api.atelbpo.com/"
    static let socket = ""
    #elseif QA
    static let server = "http://api.atelbpo.com/"
    static let socket = ""
    #elseif STAGING
    static let server = "http://api.atelbpo.com/"
    static let socket = ""
    #else
    static let server = "http://api.atelbpo.com/"
    static let socket = ""
    #endif
}
