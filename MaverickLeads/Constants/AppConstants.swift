//
//  AppConstants.swift
//  MaverickLeads
//
//  Created by Developer on 13/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit


typealias Email = String

class AppConstants {
    
    static let instaBugKey = ""
    
    static var googleClientID: String {
        switch AppTarget.current {
        case .development: return ""
        case .qa: return ""
        case .staging: return ""
        case .appStore: return ""
        }
    }
    
    struct Segues {
        struct Main {
            
        }
    }
    
    struct Storyboards {
        static let login = UIStoryboard(name: "Login", bundle: nil)
        static let leads = UIStoryboard(name: "Leads", bundle: nil)
    }
    
    struct Tags {
        
    }
    
    struct AppOpenURLConstants {
        static let reset = "/resetPassword"
        static let resetParamCode = "code"
    }
    
    struct message {
        static let noResult = "No Result Found!"
        static let contactUs = "Hi,\n\nI would like to help to improve your app experience. Here are some ways that can help:"
    }
    
    struct characterLimitation {
        static var special = "!#$%^&*()?.,<>'“;:/[]{}\\|=+-_@`~"
    }
}

enum UserRole {
    case superAdmin, teamLead, agencyOwner, agencyProducer
    
    var displayName: String {
        switch self {
        case .superAdmin:
            return "Super Admin"
        case .teamLead:
            return "Team Lead"
        case .agencyOwner:
            return "Agency Owner"
        case .agencyProducer:
            return "Agency Producer"
        }
    }
    
    static func roleFromString(role: String) -> UserRole {
        switch role {
        case "Super Admin":
            return .superAdmin
        case "Team Lead":
            return .teamLead
        case "Agency Owner":
            return .agencyOwner
        case "Agency Producer":
            return .agencyProducer
            
        default: return .agencyProducer
        }
    }
}

enum UserSuperAccess {
    case liveTransfers, liveLeads, newLead, leads, pingAndPost, outsourcePanel, logout, livePnp
    
    var displayText: String {
        switch self {
        case .liveLeads:
            return "Live Leads"
        case .livePnp:
            return "Pulse"
        case .newLead:
            return "New Lead"
        case .leads:
            return "Leads"
        case .pingAndPost:
            return "Ping"
        case .outsourcePanel:
            return "Outsource Panel"
        case .logout:
            return "Logout"
        case .liveTransfers:
            return "Live Transfers"
        }
    }
    
    var icon: UIImage {
        switch self {
        case .liveTransfers:
            return UIImage(named: "headphone")!
        case .livePnp:
        return UIImage(named: "live")!
        case .liveLeads:
            return UIImage(named: "headphone")!
        case .newLead:
            return UIImage(named: "live")!
        case .leads:
            return UIImage(named: "live")!
        case .pingAndPost:
            return UIImage(named: "pnpLogo")!
        case .outsourcePanel:
            return UIImage(named: "outsource")!
        case .logout:
            return UIImage(named: "logout")!
        }
    }
    
}

enum UserSubAccess {
    case liveTransfers, agentReport, tlNewLeads, agencyReport, pingAndPost, pnpAgencyReport, pnpAgentReport, none
    //pnpTl, liveTl, pnpAgent, liveAgent, pnpAgency, liveAgency, outsource, pnp, live
    
    var displayText: String {
        switch self {
        case .liveTransfers:
            return "Live Transfers"
        case .agentReport:
            return "Agent Report"
        case .tlNewLeads:
            return "TL New Leads"
        case .agencyReport:
            return "Agency Report"
        case .pingAndPost:
            return "Ping"
        case .pnpAgencyReport:
            return "PNP Agency Report"
        case .pnpAgentReport:
            return "PNP Agent Report"
        case .none:
            return ""
        }
    }
    
    var icon: UIImage {
        switch self {
        case .agencyReport:
            return UIImage(named: "outsource")!
        case .liveTransfers:
            return UIImage(named: "headphone")!
        case .agentReport:
            return UIImage(named: "client")!
        case .tlNewLeads:
            return UIImage(named: "newLead")!
        case .pingAndPost:
            return UIImage(named: "pnpLogo")!
        case .pnpAgencyReport:
            return UIImage(named: "outsource")!
        case .pnpAgentReport:
            return UIImage(named: "client")!
        case .none:
            return UIImage(named: "outsource")!
        }
    }
    
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
