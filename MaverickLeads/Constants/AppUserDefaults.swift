//
//  AppUserDefaults.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

struct UserDefaultObject<Value> {
    
    var key: String
    private var value: Value?
    
    init(key k: String) {
        key = k
    }
    
    mutating func clear() {
        value = nil
        UserDefaults.standard.removeObject(forKey: key)
    }
}

extension UserDefaultObject where Value: Codable {
    mutating func get() -> Value? {
        if value == nil {
            let decoder = JSONDecoder()
            if let data = UserDefaults.standard.data(forKey: key),
                let val = try? decoder.decode(Value.self, from: data) {
                value = val
            }
        }
        return value
    }
    
    mutating func set(value val: Value?) {
        value = val
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
            UserDefaults.standard.set(encoded, forKey: key)
        }
    }
}

extension UserDefaultObject where Value == String {
    
    mutating func get() -> Value? {
        if value == nil {
            value =  UserDefaults.standard.string(forKey: key)
        }
        return value
    }
    
    mutating func set(value val: Value) {
        value = val
        UserDefaults.standard.set(value, forKey: key)
    }
    
}

extension UserDefaultObject where Value == Int {
    mutating func get() -> Value? {
        if value == nil {
            value =  UserDefaults.standard.integer(forKey: key)
        }
        return value
    }
    
    mutating func set(value val: Value) {
        value = val
        UserDefaults.standard.set(value, forKey: key)
    }
}

extension UserDefaultObject where Value == Double {
    mutating func get() -> Value? {
        if value == nil {
            value =  UserDefaults.standard.double(forKey: key)
        }
        return value
    }
    
    mutating func set(value val: Value) {
        value = val
        UserDefaults.standard.set(value, forKey: key)
    }
    
}

extension UserDefaultObject where Value == Bool {
    
    mutating func set(value val: Value) {
        value = val
        UserDefaults.standard.set(value, forKey: key)
    }
    
    mutating func get() -> Value {
        if value == nil {
            value =  UserDefaults.standard.bool(forKey: key)
        }
        return value ?? false
    }

}

extension UserDefaultObject where Value == Date {
   
    mutating func get() -> Value? {
        
        if value == nil {
            var doubleValue = UserDefaultObject<Double>(key: key)
            if let timeInterval = doubleValue.get() {
                if timeInterval != 0 {
                    value = Date(timeIntervalSince1970: timeInterval)
                }
            }
        }
        return value
    }
    
    mutating func set(value val: Value) {
        value = val
        UserDefaults.standard.set(value!.timeIntervalSince1970, forKey: key)
    }
}

extension UserDefaultObject where Value == [String] {
    
    mutating func get() -> Value? {
        if value == nil {
            if let array = UserDefaults.standard.array(forKey: key) as? [String] {
                debugPrint("Array: \(array)")
                value = array
            }
        }
        return value
    }
    
    mutating func append(value: String) {
        var array: [String] = []
        if let tempArray = get() {
            array.append(contentsOf: tempArray)
        }
        array.append(value)
        UserDefaults.standard.set(array, forKey: key)
    }
    
}

struct AppUserDefaults {
    static var user = UserDefaultObject<MLUser>(key: "User")
    static var leads = UserDefaultObject<[MLLead]>(key: "Leads")
    static var dispositionList = UserDefaultObject<[MLDisposition]>(key: "dispositionList")
    static var producerList = UserDefaultObject<[MLProducer]>(key: "producerList")
    static var pushToken = UserDefaultObject<String>(key: "pushToken")
}
