//
//  FBHandler.swift
//  CRSignup
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Muhammad Developer. All rights reserved.
//

import Foundation

class FBHandler {
    
    
    // MARK: - Class Properties
    
    static var shared: FBHandler = FBHandler()
    fileprivate var allowedPermissions: [FBPermissions] = []
    fileprivate let loginManager: LoginManager
    
    
    // MARK: - Initialization Methods
    
    fileprivate init() {
        loginManager = LoginManager()
    }
    
    
    // MARK: - Class Methods
    
    func login(with permissions: [FBPermissions], from viewController: UIViewController, completion: @escaping SocialSignInCompletion) {
        var readPermissions: [String] = []
        if !permissions.isEmpty {
            permissions.forEach { readPermissions.append($0.value) }
        }
        
        loginManager.logIn(permissions: readPermissions, from: viewController) { [weak self] result, error in
            guard let self = self else { return }
            if error != nil {
                debugPrint(error ?? "ERROR")
                completion(nil)
            } else {
                guard let result = result else {
                    completion(nil)
                    return
                }
                if result.isCancelled {
                    completion(nil)
                } else {
                    debugPrint(result)
                    self.allowedPermissions = permissions
                    self.fetchFacebookUser(completion: completion)
                }
            }
        }
    }
    
    func logout() {
        loginManager.logOut()
    }
    
    func fetchFacebookUser(completion: @escaping SocialSignInCompletion) {
        var readPermissions: [String] = []
        if !allowedPermissions.isEmpty {
            if allowedPermissions.contains(.email) {
                readPermissions.append("email")
            }
            if allowedPermissions.contains(.publicProfile) {
                readPermissions.append("first_name")
                readPermissions.append("last_name")
                readPermissions.append("birthday")
                readPermissions.append("id")
            }
        }
        
        var parameters: [String: String] = [:]
        if !readPermissions.isEmpty {
            let paramsString = readPermissions.joined(separator: ",")
            parameters["fields"] = paramsString
        }
        
        let userRequest = GraphRequest(graphPath: "me", parameters: parameters)
        userRequest.start { [weak self] connection, result, error in
            guard let self = self else { return }
            if error != nil {
                debugPrint(error!)
                completion(nil)
            } else {
                if let dictionary = result as? [String : Any] {
                    let fbUser = FBUser(dictionary: dictionary)
                    let socialUser = SocialNetworkUser(user: fbUser)
                    completion(socialUser)
                    self.logout()
                } else {
                    completion(nil)
                }
            }
        }
    }
}
