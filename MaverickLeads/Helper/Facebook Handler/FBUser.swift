//
//  FBUser.swift
//  MaverickLeads
//
//  Created by Developer on 15/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

struct FBUser {
    
    
    // MARK: - Class Properties
    
    var userId: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var birthday: String?
    
    
    // MARK: - Initialization Methods
    
    init(dictionary: [String : Any]) {
        userId = dictionary["id"] as? String
        firstName = dictionary["first_name"] as? String
        lastName = dictionary["last_name"] as? String
        email = dictionary["email"] as? String
        birthday = dictionary["birthday"] as? String
    }
    
}
