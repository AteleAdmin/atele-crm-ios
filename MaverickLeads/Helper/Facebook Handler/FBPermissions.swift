//
//  FBPermissions.swift
//  CRSignup
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Muhammad Developer. All rights reserved.
//

import Foundation

enum FBPermissions {
    case publicProfile
    case email
    
    var value: String {
        switch self {
        case .publicProfile:
            return "public_profile"
        case .email:
            return "email"
        }
    }
}
