//
//  AppNavigationController.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

class AppNavigationController: UINavigationController {
    
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        setupNavigationBar()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}


// MARK: - Private Methodss

extension AppNavigationController {
    
    private func setupNavigationBar(itemColor: UIColor = .white) {
        navigationBar.barTintColor = .blue
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.appFront(ofSize: 18, type: .title), NSAttributedString.Key.foregroundColor: itemColor]
    }
}
