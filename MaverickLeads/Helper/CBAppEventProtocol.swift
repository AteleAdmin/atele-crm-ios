//
//  CBAppEventProtocol
//  PelicanWater
//
//  Created by Developer  on 26/10/2018.
//  Copyright © 2018 Developer . All rights reserved.
//

import Foundation
import UIKit

@objc enum CBAppDelegateEvent:Int, CaseIterable {
    case didBecomeActive = 0, willEnterForeground, didEnterBackground, windowBecomeVisible, windowBecomeHidden, willResignActive
    
    var notificationSystemName: NSNotification.Name {
        switch self {
        case .didBecomeActive: return UIApplication.didBecomeActiveNotification
        case .willEnterForeground: return UIApplication.willEnterForegroundNotification
        case .didEnterBackground: return UIApplication.didEnterBackgroundNotification
        case .windowBecomeHidden: return UIWindow.didBecomeHiddenNotification
        case .windowBecomeVisible: return UIWindow.didBecomeVisibleNotification
        case .willResignActive: return UIApplication.willResignActiveNotification
        }
    }
}

protocol CBAppEventProtocol {
    func onAppStateChange(_ state: CBAppDelegateEvent)
}

extension CBAppEventProtocol where Self: UIViewController {
   
    func configureAppEvents(for notification_types: [CBAppDelegateEvent]) {
        
        notification_types.forEach { (anEventType) in
            NotificationCenter.default.addObserver(forName: anEventType.notificationSystemName, object: nil, queue: nil) { [weak self] (notification) in
                self?.onAppStateChange(anEventType)
            }
        }
    
    }
    
}
