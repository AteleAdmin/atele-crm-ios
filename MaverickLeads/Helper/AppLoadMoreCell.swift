//
//  AppLoadMoreCell.swift
//  MaverickLeads
//
//  Created by Developer on 18/06/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit
import SwipeCellKit

class AppLoadMoreCell<U>: SwipeTableViewCell, Registerable {

    
    // MARK: - Class Properties
    
    var item: U!
    
    
    // MARK: - Initialization Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
