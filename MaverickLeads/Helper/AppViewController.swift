//
//  AppViewController.swift
//  MaverickLeads
//
//  Created by Developer on 14/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit
import MessageUI

class AppViewController: UIViewController, ErrorHandler {

    
    // MARK: - Class Properties
    
    lazy var segueManager: SegueManager = { return SegueManager(viewController: self) }()
    private var adBannerStateUpdated: CRGADStateHandler?
    @IBInspectable var dismissKeyboardOnTouch: Bool = true
    
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if dismissKeyboardOnTouch {
            view.endEditing(false)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}


// MARK: - Public Methods

extension AppViewController {
    
    
    func addLoadingView() {
        let loadingView = UIView()
        loadingView.tag = 30003
        view.addSubview(loadingView)
        loadingView.snp.makeConstraints {
            $0.width.equalToSuperview()
            $0.height.equalToSuperview()
            $0.center.equalToSuperview()
        }
        loadingView.backgroundColor = .white
        view.bringSubviewToFront(loadingView)
    }
    
    func removeLoadingView() {
        if let loadingView = view.viewWithTag(30003) {
            loadingView.removeFromSuperview()
        }
    }
    
    func shareApp() {
        let appStoreLink = "https://itunes.apple.com/app/id"
        guard let url = URL(string: appStoreLink) else { return }
        let message = "Check out my new app!\n\nCome and join me."
        let shareObjects: [Any] = [message, url]
        let activityVC = UIActivityViewController(activityItems: shareObjects, applicationActivities: nil)
        present(activityVC, animated: true, completion: nil)
    }
    
    func contactUs() {
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setSubject(NSLocalizedString("Champions Feedback", comment: ""))
            let email = ""
            composeVC.setToRecipients([email])
            composeVC.setMessageBody(AppConstants.message.contactUs, isHTML: false)
            present(composeVC, animated: true, completion: nil)
        } else {
            // Show Alert
        }
    }
    
    func showEmailComposer(withEmail email: String) {
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setSubject(NSLocalizedString("Contact", comment: ""))
            composeVC.setToRecipients([email])
            present(composeVC, animated: true, completion: nil)
        } else {
            // Do nothing for now
        }
    }
}


// MARK: - Private Methodss

extension AppViewController {

    private func setupNavigationBar() {
        if let navController = navigationController {
            navController.navigationBar.tintColor = .white
            navController.navigationBar.barTintColor = .white
            navController.navigationBar.isTranslucent = false
            navController.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.appFront(ofSize: 18, type: .title), NSAttributedString.Key.foregroundColor: UIColor.white]
            navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navController.navigationBar.shadowImage = UIImage()
            navController.isNavigationBarHidden = false
            navController.navigationBar.backIndicatorImage = UIImage(named: "arrowLeft")
            navController.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrowLeft")
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            
            view.backgroundColor = .white
        }
    }
    
}


// MARK: - SeguePerformer Methods

extension AppViewController: SeguePerformer {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segueManager.prepare(for: segue)
    }
}


// MARK: - MFMailComposeViewController Methods

extension AppViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


// ---------------------------- Google Ad Extension

typealias CRGADStateHandler = (CRGADBannerState) -> ()

enum CRGADBannerState {
    case didReceive, willPresent, willDismiss, didDismiss, willLeaveApp, didFailToReceive
}


