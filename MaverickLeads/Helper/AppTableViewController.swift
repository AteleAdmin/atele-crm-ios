//
//  AppTableViewController.swift
//  MaverickLeads
//
//  Created by Developer on 20/05/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit

class AppTableViewController: UITableViewController, ErrorHandler {

    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupNavigationBar()
    }

}


extension AppTableViewController {
    
    private func setupNavigationBar() {
        if let navController = navigationController {
            navController.navigationBar.tintColor = .white
            navController.navigationBar.barTintColor = .white
            navController.navigationBar.isTranslucent = false
            navController.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.appFront(ofSize: 18, type: .title), NSAttributedString.Key.foregroundColor: UIColor.white]
            navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navController.navigationBar.shadowImage = UIImage()
            navController.isNavigationBarHidden = false
            navController.navigationBar.backIndicatorImage = UIImage(named: "arrowLeft")
            navController.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrowLeft")
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            
            view.backgroundColor = .white
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
