//
//  File.swift
//  JvstFamovs
//
//  Created by ZaeemZafar on 14/07/2018.
//  Copyright © 2018 company. All rights reserved.
//

import Foundation
import UserNotifications
import MobileCoreServices
import UIKit

let navigationDelay: Double = 100

struct Notifications {
    static let push = NSNotification.Name("push")
    static let configurationUpdated = NSNotification.Name("configurationUpdated")
}

let gcmMessageKey = "gcm.message_id"
typealias CBNotificationResponseHandler = ((_ data: CBPushNotificationData) -> Void)

class CBNotificationManager: NSObject {
    static let shared = CBNotificationManager()
    
    private var notificationData: CBPushNotificationData? = nil
    var observerHandled = false
    func configure() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        setupPushNotification()
    }
    
    func setupPushNotification() {
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { success, _ in
                if success {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
        })
    }
    
    func notificationManager(didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print(#function)
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNS token is \(deviceTokenString)")
        
        AppUserDefaults.pushToken.set(value: deviceTokenString)
        
//        if AppTarget.current != .appStore {
            let copyString = "APNS token is \(deviceTokenString)\n\nOn Date: \(Date())"
            UIPasteboard.general.string = copyString
//        }
    }
    
    func notificationManager(didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error occured while registering for push \(error)")
    }
    
    // Silent/Data only notification handler
    func didReceiveRemoteNotification(_ userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // Print full message.
        print("FCM Notification Data \(userInfo)")
        
        let notificationData = CBNotificationManager.getNotificationDataFromPayload(payload: userInfo, fromNotificationBanner: false)
        
        DispatchTime.executeAfter(milliSeconds: navigationDelay, completionHandler: {
            NotificationCenter.default.post(name: Notifications.push, object: notificationData)
        })
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func popPushNotification() -> CBPushNotificationData? {
        let data = notificationData
        notificationData = nil
        return data
    }
}

extension CBNotificationManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive
        response: UNNotificationResponse, withCompletionHandler
        completionHandler: @escaping () -> Void) {
        print(#function)
        let userInfo = response.notification.request.content.userInfo
 
        print("FCM Notification Data \(userInfo)")
       
        let notificationData = CBNotificationManager.getNotificationDataFromPayload(payload: userInfo, fromNotificationBanner: true)
        
        if canHandle() {
            DispatchTime.executeAfter(milliSeconds: navigationDelay, completionHandler: {
                NotificationCenter.default.post(name: Notifications.push, object: notificationData)
                completionHandler()
            })
        } else {
            self.notificationData = notificationData
        }
        
    }
    
    private func canHandle() -> Bool {
        let handle = true
        return handle
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent
        notification: UNNotification, withCompletionHandler
        completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(#function)
        let userInfo = notification.request.content.userInfo
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageKey] {
            print("FCM Message ID: \(messageID)")
        }

        // Print full message.
        print("FCM Notification Data \(userInfo)")

        let notificationData = CBNotificationManager.getNotificationDataFromPayload(payload: userInfo, fromNotificationBanner: false)
        NotificationCenter.default.post(name: Notifications.push, object: notificationData)
        
        completionHandler([.badge, .alert, .sound])
    }
    
    class func getNotificationDataFromPayload(payload: [AnyHashable: Any], fromNotificationBanner: Bool) -> CBPushNotificationData {
        let typeString = payload["type"] as? String ?? ""
        let leagueId = payload["leagueId"] as? String ?? ""
        let playerId = payload["playerId"] as? String ?? ""
        let type = CBPushNotificationType(rawValue: typeString) ?? .none
        
        return CBPushNotificationData(notification_type: type, content_id: leagueId, triggered_notification_centre: fromNotificationBanner, player_Id: playerId)
    }
}

// Notifications
extension CBNotificationManager {
    func didReceiveNotification(ofType notification_type: CBPushNotificationType, performAction: CBNotificationResponseHandler?) {
        NotificationCenter.default.addObserver(forName: Notifications.push, object: nil, queue: nil) { (notification) in
            print(#function)
            guard let notificationData = notification.object as? CBPushNotificationData else { return }
            if notificationData.type == notification_type {
                performAction?(notificationData)

            } else {
                // Do nothing…
                print("Some other type")
            }

        }
    }

    func didReceiveNotification(ofType notification_types: [CBPushNotificationType], performAction: CBNotificationResponseHandler?) {
        NotificationCenter.default.addObserver(forName: Notifications.push, object: nil, queue: nil) { (notification) in
            print(#function)

            guard let notificationData = notification.object as? CBPushNotificationData else { return }
        }
    }

    func onNotificationReceive(_ performAction: CBNotificationResponseHandler?) {
        
        if observerHandled {
            return
        }
        
        observerHandled = true
        
        NotificationCenter.default.addObserver(forName: Notifications.push, object: nil, queue: nil) { (notification) in
            print(#function)
            guard let notificationData = notification.object as? CBPushNotificationData else { return }

            DispatchTime.executeAfter(milliSeconds: navigationDelay, completionHandler: {
                performAction?(notificationData)
            })
            
        }
    }
}


class CBPushNotificationData {
    var type: CBPushNotificationType
    var openedUpFromNotificationCenter: Bool
    var contentID: String
    var playerId: String
    
    init(notification_type: CBPushNotificationType, content_id: String, triggered_notification_centre: Bool = false, player_Id: String) {
        type = notification_type
        openedUpFromNotificationCenter = triggered_notification_centre
        contentID = content_id
        playerId = player_Id
    }
}

enum CBPushNotificationType: String, Codable {
    case test
    case none
}

