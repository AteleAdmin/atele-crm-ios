//
//  PagerController.swift
//  DTPagerController
//
//  Created by tungvoduc on 22/09/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

//import DTPagerController
import UIKit

class PagerController: DTPagerController {
    
    var apiCategory: APICategory!
    var subAccess: UserSubAccess!
    
    init() {
        super.init(viewControllers: [])
    }
    
    convenience init(withTitle title_string: String, api_category: APICategory, sub_access: UserSubAccess) {
        self.init()
        
        apiCategory = api_category
        title = title_string
        subAccess = sub_access
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        perferredScrollIndicatorHeight = 4

        let viewController1: StatsViewController = instanceFromStoryboard(storyboard: AppConstants.Storyboards.leads)
        viewController1.title = "Stats"
        viewController1.apiCategory = apiCategory

        let viewController2: ExportViewController = instanceFromStoryboard(storyboard: AppConstants.Storyboards.leads)
        viewController2.title = "Leads"
        viewController2.apiCategory = apiCategory
        
        let viewController3: AgencyWiseLeadsViewController = instanceFromStoryboard(storyboard: AppConstants.Storyboards.leads)
        viewController3.title = "Agency Wise"

        if subAccess == UserSubAccess.agencyReport || subAccess == UserSubAccess.pnpAgencyReport {
            viewControllers = [viewController1, viewController3, viewController2]
        } else {
            viewControllers = [viewController1, viewController2]
        }
        
        scrollIndicator.layer.cornerRadius = scrollIndicator.frame.height / 2

        setSelectedPageIndex(0, animated: false)

        pageSegmentedControl.tintColor = UIColor.appMain
        pageSegmentedControl.backgroundColor = .white
        pageSegmentedControl.layer.masksToBounds = false
        pageSegmentedControl.layer.shadowColor = UIColor.lightGray.cgColor
        pageSegmentedControl.layer.shadowOffset = CGSize(width: 0, height: 1)
        pageSegmentedControl.layer.shadowRadius = 1
        pageSegmentedControl.layer.shadowOpacity = 0.5
    }

}
