//
//  AppLoadMoreListViewController.swift
//  MaverickLeads
//
//  Created by Developer on 18/06/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import UIKit
import SwipeCellKit
import CRRefresh

protocol LoadMoreDelegate: class {
    func loadData(pageNumber: Int, showLoader: Bool)
}

class AppLoadMoreListViewController<T: AppLoadMoreCell<U>, U>: AppTableViewController, SwipeTableViewCellDelegate {
    
    
    // MARK: - Class Properties
    
    private var items: [U] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    private var onDidSelect: ((U) -> Void)? = nil
    public var onDelete: ((U) -> Void)? = nil
    private var cellIdentifier: String = String(describing: T.self)
    private var currentPage: Int = 1
    private var pagingMetadata: PagingMetaData? = nil
    public var isLoadMoreEnabled: Bool {
        return true
    }
    public var isSwipeToDeleteEnabled: Bool {
        return true
    }
    public weak var loadMoreDelegate: LoadMoreDelegate? = nil
    
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupViewController()
    }
    
    
    // MARK: - Private Methods
    
    private func setupViewController() {
        tableView.tableFooterView = UIView(frame: .zero)
        addTableHeaderRefresh()
        if isLoadMoreEnabled {
            addTableFooterRefresh()
        }
    }
    
    private func addTableHeaderRefresh() {
        tableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            guard let self = self else { return }
            self.currentPage = 1
            self.loadData(pageNumber: self.currentPage, showLoader: false)
        }
    }
    
    private func addTableFooterRefresh() {
        tableView.cr.addFootRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            guard let self = self else { return }
            self.loadMore()
        }
    }
    
    private func loadMore() {
        guard let pagingMetadata = pagingMetadata else { return }
        if currentPage < pagingMetadata.totalPages {
            loadData(pageNumber: self.currentPage, showLoader: false)
        } else {
            tableView.cr.removeFooter()
        }
    }
    
    private func dataSourceUpdated() {
        // Check for empty data state and update view accordingly
        items.count > 0 ? tableView.resetBackgroundView() : tableView.setEmptyBackgroundView(text: AppConstants.message.noResult)
    }
    
    private func loadData(pageNumber: Int, showLoader: Bool) {
        guard let delegate = loadMoreDelegate else { return }
        delegate.loadData(pageNumber: pageNumber, showLoader: showLoader)
    }
    
    private func updateUi() {
        if isLoadMoreEnabled {
            currentPage += 1
            tableView.cr.endLoadingMore()
            if let metadata = pagingMetadata,
                currentPage >= metadata.totalPages {
                tableView.cr.removeFooter()
            } else {
                if self.items.count > 0 {
                    addTableFooterRefresh()
                }
            }
        }
        tableView.cr.endHeaderRefresh()
        tableView.reloadData()
        dataSourceUpdated()
    }

    
    // MARK: - Public Methods
    
    public func update(items: [U], metadata: PagingMetaData?, onSelect: ((U) -> Void)? = nil) {
        pagingMetadata = metadata
        if currentPage == 1 {
            self.items.removeAll()
        }
        self.items.append(contentsOf: items)
        onDidSelect = onSelect
        updateUi()
    }
    
    public func reset() {
        self.items.removeAll()
        updateUi()
    }
    
    
    // MARK: - TableView Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AppLoadMoreCell<U>
        
        if isSwipeToDeleteEnabled {
            cell.delegate = self
        }
        let item = items[indexPath.row]
        cell.item = item
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        cell.separatorInset = .zero
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let onDidSelect = onDidSelect else { return }
        let item = items[indexPath.row]
        onDidSelect(item)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .default, title: nil) { [weak self] action, indexPath in
            guard let self = self else { return }
            // handle action by updating model with deletion
            if let onDelete = self.onDelete {
                let item = self.items[indexPath.row]
                onDelete(item)
            }
            self.items.remove(at: indexPath.row)
            self.dataSourceUpdated()
            action.fulfill(with: .delete)
        }
        deleteAction.image = UIImage(named: "delete_black")
        deleteAction.backgroundColor = .groupTableViewBackground
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .border
        return options
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
}
