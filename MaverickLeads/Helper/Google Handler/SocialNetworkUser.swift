//
//  SocialNetworkUser.swift
//  MaverickLeads
//
//  Created by Developer on 15/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation

typealias SocialSignInCompletion = ((SocialNetworkUser?) -> Void)

struct SocialNetworkUser {
    
    
    // MARK: - Class Properties
    
    var googleId: String? = nil
    var facebookId: String? = nil
    var firstName: String? = nil
    var lastName: String? = nil
    var email: String? = nil
    
    
    // MARK: - Initialization Methods
    
    init(user: GIDGoogleUser) {
        googleId = user.userID
        firstName = user.profile.name.firstName
        lastName = user.profile.name.lastName
        email = user.profile.email
    }
    
    init(user: FBUser) {
        if let fName = user.firstName, !fName.isEmpty {
            firstName = fName
        }
        
        if let lName = user.lastName, !lName.isEmpty {
            lastName = lName
        }
        
        if let userEmail = user.email {
            email = userEmail
        }
        
        facebookId = user.userId
    }
    
}
