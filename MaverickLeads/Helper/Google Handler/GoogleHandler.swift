//
//  GoogleHandler.swift
//  MaverickLeads
//
//  Created by Developer on 15/03/2019.
//  Copyright © 2019 Developer. All rights reserved.
//

import Foundation
import GoogleSignIn

class GoogleHandler: NSObject {
    
    
    // MARK: - Class Properties
    
    static let shared: GoogleHandler = GoogleHandler()
    fileprivate var presentingViewController: UIViewController?
    fileprivate var socialCompletion: SocialSignInCompletion?
    
    
    // MARK: - Initialization Methods
    
    fileprivate override init() {
        super.init()
        GIDSignIn.sharedInstance()!.delegate = self
    }
    
    
    // MARK: - Public Methods
    
    public func login(from viewController: UIViewController, completion: @escaping SocialSignInCompletion) {
        presentingViewController = viewController
        socialCompletion = completion
        GIDSignIn.sharedInstance()?.presentingViewController = presentingViewController
        GIDSignIn.sharedInstance()!.signIn()
    }
}


// MARK: - GIDSignInDelegate Methods

extension GoogleHandler: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let socialCompletion = socialCompletion else { return }
        if error != nil {
            socialCompletion(nil)
        } else {
            let socialUser = SocialNetworkUser(user: user)
            socialCompletion(socialUser)
            
            // Developer's Note: We don't want Google SDK to maintain session for app anymore
            // e.g. User may want to use different Google email anytime
            GIDSignIn.sharedInstance()?.signOut()
        }
    }
    
}
